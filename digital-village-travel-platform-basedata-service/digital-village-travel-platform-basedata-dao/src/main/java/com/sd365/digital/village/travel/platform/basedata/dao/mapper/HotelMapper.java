package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.Hotel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface HotelMapper extends CommonMapper<Hotel> {

    /**
     * 根据查询条件查询酒店列表
     * @param hotel
     * @return
     */
    List<Hotel> commonQuery(Hotel hotel);


    /**
     * 根据id查询某一酒店信息
     * @param id
     * @return
     */
    Hotel selectById(Long id);

    /**
     * pc端根据查询条件查询酒店列表
     * @param hotel
     * @return
     */
    List<Hotel> pcCommonQuery(Hotel hotel);
}
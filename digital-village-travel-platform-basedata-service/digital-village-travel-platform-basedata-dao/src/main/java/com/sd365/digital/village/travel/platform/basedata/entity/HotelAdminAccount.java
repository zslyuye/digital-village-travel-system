package com.sd365.digital.village.travel.platform.basedata.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

@ApiModel(value="com.sd365.digital.village.travel.platform.basedata.entity.HotelAdminAccount")
@Table(name = "hotel_admin_account")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HotelAdminAccount extends TenantBaseEntity {
    /**
     * 酒店管理员账号
     */
    @ApiModelProperty(value="account酒店管理员账号")
    private String account;

    /**
     * 酒店管理员密码
     */
    @ApiModelProperty(value="password酒店管理员密码")
    private String password;

    /**
     * 酒店管理员名字
     */
    @ApiModelProperty(value="name酒店管理员名字")
    private String name;

    /**
     * 酒店管理员联系方式
     */
    @ApiModelProperty(value="phone酒店管理员联系方式")
    private String phone;

    /**
     * 管理员所属酒店id
     */
    @ApiModelProperty(value="hotelId管理员所属酒店id")
    private Long hotelId;

    /**
     * 管理员所属酒店
     */
    @ApiModelProperty(value="管理员所属酒店")
    private Hotel hotel;

    /**
     * 所属供应商
     */
    @ApiModelProperty(value="所属供应商")
    private Supplier supplier;
}
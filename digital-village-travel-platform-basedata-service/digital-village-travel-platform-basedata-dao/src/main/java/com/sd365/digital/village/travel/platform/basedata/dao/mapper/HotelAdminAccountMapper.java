package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.digital.village.travel.platform.basedata.entity.HotelAdminAccount;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface HotelAdminAccountMapper extends Mapper<HotelAdminAccount> {
    List<HotelAdminAccount> commonQuery(HotelAdminAccount hotelAdminAccount);

    HotelAdminAccount queryByAccount(@Param("account") String account);
}
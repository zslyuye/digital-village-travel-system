package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.HotelRoom;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface HotelRoomMapper extends CommonMapper<HotelRoom> {

    /**
     * 根据条件查询酒店房间
     * @param hotelRoom
     * @return
     */
    List<HotelRoom> selectHotelRoom(HotelRoom hotelRoom);

    Boolean decreaseRoom(Long hotelId, Long id, Integer count);
}
package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.digital.village.travel.platform.basedata.entity.Member;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface MemberMapper extends Mapper<Member> {
    List<Member> commonQuery(Member member);

    Member queryByAccount(@Param("account") String acount);
}
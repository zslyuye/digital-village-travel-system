package com.sd365.digital.village.travel.platform.basedata.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@ApiModel(value="com.sd365.digital.village.travel.platform.basedata.entity.AttractionTicket")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "attraction_ticket")
public class AttractionTicket extends TenantBaseEntity {
    /**
     * 景点id
     */
    @ApiModelProperty(value="attractionId景点id")
    private Long attractionId;

    /**
     * 价格
     */
    @ApiModelProperty(value="price价格")
    private Float price;

    /**
     * 销量
     */
    @ApiModelProperty(value="saleNum销量")
    private Integer saleNum;

    /**
     * 门票名称
     */
    @ApiModelProperty(value="name门票名称")
    private String name;
    /**
     * 门票信息
     */
    @ApiModelProperty(value="name门票信息描述")
    private String info;
    /**
     * 景点
     */
    @Transient
    @ApiModelProperty(value="景点属性")
    private Attraction attraction;
}
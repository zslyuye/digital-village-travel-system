package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.digital.village.travel.platform.basedata.entity.AttractionAdminAccount;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface AttractionAdminAccountMapper extends Mapper<AttractionAdminAccount> {
    List<AttractionAdminAccount> commonQuery(AttractionAdminAccount attractionAdminAccount);

    AttractionAdminAccount queryByAccount(@Param("account") String account);
}
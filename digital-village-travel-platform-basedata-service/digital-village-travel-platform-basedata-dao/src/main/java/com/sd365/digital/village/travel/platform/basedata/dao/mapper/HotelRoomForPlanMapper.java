package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.HotelRoomForPlan;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface HotelRoomForPlanMapper extends CommonMapper<HotelRoomForPlan> {

    /**
     * 根据条件定制计划查询酒店房间价格
     *
     * @param hotelRoomForPlan
     * @return
     */
    List<HotelRoomForPlan> commonQuery(HotelRoomForPlan hotelRoomForPlan);
}
package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.digital.village.travel.platform.basedata.entity.Picture;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface PictureMapper extends Mapper<Picture> {
    /**
     *  根据状态或者顾客名称 使用多表链接查询查找Picture对象
     * @param picture
     * @return AttractionTicket对象集合
     */
    List<Picture> commonQuery(Picture picture);
    /**
     *  根据id 使用多表链接查询查找Picture对象
     * @param id  picture的 id
     * @return Picture对象
     */
    Picture selectById(Long id);
}
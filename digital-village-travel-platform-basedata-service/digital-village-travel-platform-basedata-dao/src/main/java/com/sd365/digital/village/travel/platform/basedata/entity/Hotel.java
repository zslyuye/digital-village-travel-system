package com.sd365.digital.village.travel.platform.basedata.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.List;

@ApiModel(value="com.sd365.digital.village.travel.platform.basedata.entity.Hotel")
@Table(name = "hotel")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hotel extends TenantBaseEntity {
    /**
     * 酒店供应商id
     */
    @ApiModelProperty(value="hotelSupplierId酒店供应商id")
    private Long hotelSupplierId;

    /**
     * 酒店名称
     */
    @ApiModelProperty(value="name酒店名称")
    private String name;

    /**
     * 酒店分类（0经济型、1舒适性、2商务型）
     */

    @ApiModelProperty(value="type酒店分类")
    private Byte type;

    /**
     * 参考价格
     */
    @ApiModelProperty(value="参考价格")
    private Float referencePrice;

    /**
     * 酒店地址
     */
    @ApiModelProperty(value="address酒店地址")
    private String address;

    /**
     * 经度
     */
    @ApiModelProperty(value="longitude经度")
    private Double longitude;

    /**
     * 维度
     */
    @ApiModelProperty(value="latitude维度")
    private Double latitude;

    /**
     * 酒店简介
     */
    @ApiModelProperty(value="introduction酒店简介")
    private String introduction;

    /**
     * 酒店是否营业
     */
    @ApiModelProperty(value="open酒店是否营业")
    private Boolean open;

    /**
     * 酒店负责人
     */
    @ApiModelProperty(value="boss酒店负责人")
    private String boss;

    /**
     * 酒店负责人手机号
     */
    @ApiModelProperty(value="tel酒店负责人手机号")
    private String tel;

    /**
     * 酒店收益账户
     */
    @ApiModelProperty(value="bankAccount酒店收益账户")
    private String bankAccount;

    /**
     * 酒店评分
     */
    @ApiModelProperty(value="score酒店评分")
    private Float score;

    /**
     * 酒店星级
     */
    @ApiModelProperty(value="star酒店星级")
    private Integer star;

    /**
     * 酒店收益总额
     */
    @ApiModelProperty(value="profitSum酒店收益总额")
    private Float profitSum;

    /**
     * 顾客总数
     */
    @ApiModelProperty(value="customerSum顾客总数")
    private Integer customerSum;

    /**
     * 未处理订单总数
     */
    @ApiModelProperty(value="unsolveOrder未处理订单总数")
    private Integer unsolveOrder;

    /**
     * 在售门票种类
     */
    @ApiModelProperty(value="roomTypeSum在售房型种类")
    private Integer roomTypeSum;


    /**
     * 酒店所属供应商
     */
    @ApiModelProperty(value="酒店所属供应商")
    private Supplier supplier;

    /**
     * 景点图片
     */
    @ApiModelProperty(value="景点图片")
    private List<Picture> pictures;
}
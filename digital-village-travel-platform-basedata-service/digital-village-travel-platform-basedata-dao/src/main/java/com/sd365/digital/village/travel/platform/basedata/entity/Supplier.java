package com.sd365.digital.village.travel.platform.basedata.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@ApiModel(value="com.sd365.digital.village.travel.platform.basedata.entity.Supplier")
@Table(name = "supplier")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Supplier extends TenantBaseEntity {
    /**
     * 供应商类型
     */
    @ApiModelProperty(value="type供应商类型")
    private Byte type;

    /**
     * 供应商名称
     */
    @ApiModelProperty(value="name供应商名称")
    private String name;

    /**
     * 供应商帐号
     */
    @ApiModelProperty(value="code供应商帐号")
    private String code;

    /**
     * 密码
     */
    @ApiModelProperty(value="password密码")
    private String password;

    /**
     * 负责人
     */
    @ApiModelProperty(value="boss负责人")
    private String boss;

    /**
     * 联系方式
     */
    @ApiModelProperty(value="tel联系方式")
    private String tel;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    private String email;

    /**
     * 联系地址
     */
    @ApiModelProperty(value="address联系地址")
    private String address;

    /**
     * 营业执照图片
     */
    @ApiModelProperty(value="营业执照图片")
    private List<Picture> pictures;

}
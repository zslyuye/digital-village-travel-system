package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.AttractionTicketForPlan;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AttractionTicketForPlanMapper extends CommonMapper<AttractionTicketForPlan> {

    /**
     * 根据条件查询定制计划景点门票价格
     *
     * @param attractionTicketForPlan
     * @return
     */
    List<AttractionTicketForPlan> commonQuery(AttractionTicketForPlan attractionTicketForPlan);
}
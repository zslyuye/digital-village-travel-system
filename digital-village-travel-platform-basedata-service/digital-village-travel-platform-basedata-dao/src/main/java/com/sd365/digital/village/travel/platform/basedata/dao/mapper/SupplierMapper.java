package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.digital.village.travel.platform.basedata.entity.Supplier;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SupplierMapper extends Mapper<Supplier> {


    /**
     * 根据查询条件查询供应商列表
     * @param supplier
     * @return 供应商列表
     */
    List<Supplier> commonQuery(Supplier supplier);


    /**
     * 根据id查询某一供应商信息
     * @param id
     * @return 查询出的供应商信息
     */
    Supplier selectById(Long id);
}
package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.Attraction;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AttractionMapper extends CommonMapper<Attraction> {


    /**
     * 根据查询条件查询景点列表
     * @param attraction
     * @return
     */
    List<Attraction> commonQuery(Attraction attraction);


    /**
     * 根据id查询某一景点信息
     * @param id
     * @return
     */
    Attraction selectById(Long id);

    /**
     * pc端根据查询条件查询景点列表
     * @param attraction
     * @return
     */
    List<Attraction> pcCommonQuery(Attraction attraction);
}
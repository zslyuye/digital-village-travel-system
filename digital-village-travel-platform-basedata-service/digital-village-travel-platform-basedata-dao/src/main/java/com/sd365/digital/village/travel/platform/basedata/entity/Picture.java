package com.sd365.digital.village.travel.platform.basedata.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@ApiModel(value="com.sd365.digital.village.travel.platform.basedata.entity.Picture")
@Table(name = "picture")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Picture extends TenantBaseEntity {
    /**
     * 景点、酒店或者其他需要关联图片的表的主键
     */
    @ApiModelProperty(value="fkId景点、酒店或者其他需要关联图片的表的主键")
    private Long fkId;

    /**
     * 图片的url
     */
    @ApiModelProperty(value="url图片的url")
    private String url;

}
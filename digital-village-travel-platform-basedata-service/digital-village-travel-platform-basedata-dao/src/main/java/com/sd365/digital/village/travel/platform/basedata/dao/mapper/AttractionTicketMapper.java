package com.sd365.digital.village.travel.platform.basedata.dao.mapper;

import com.sd365.digital.village.travel.platform.basedata.entity.AttractionTicket;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface AttractionTicketMapper extends Mapper<AttractionTicket> {
    /**
     *  根据状态或者顾客名称 使用多表链接查询查找AttractionTicket对象
     * @param attractionTicket
     * @return AttractionTicket对象集合
     */
    List<AttractionTicket> commonQuery(AttractionTicket attractionTicket);
    /**
     *  根据id 使用多表链接查询查找AttractionTicket对象
     * @param id  attractionTicket的 id
     * @return 带有 公司 机构  结算类型等字段的AttractionTicket对象
     */
    AttractionTicket selectById(Long id);
}
package com.sd365.digital.village.travel.platform.basedata.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class SendEmailUtil {

    /**
     * 发送者
     */
    @Value("${mail.fromMail.sender}")
    private String sender;

    @Autowired
    private JavaMailSender javaMailSender;

    public Boolean sendEmail(String email, Byte status, String checkMessage) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(sender);
        message.setTo(email);
        message.setSubject("智慧乡村-旅游供应商账号审核通知");
        if (status == 1) {
            // 内容
            message.setText("【智慧乡村-旅游系统】你的供应商账号已经审核通过了，请登录供应商管理后台登录验证(若你已经知晓，可忽略该条邮件)");
        } else {
            // 内容
            message.setText("【智慧乡村-旅游系统】你的供应商账号审核不通过，不通过信息:" + checkMessage + ",请登录供应商管理后台按照要求重新注册(若你已经知晓，可忽略该条邮件)");
        }

        try {
            javaMailSender.send(message);
            return true;
        } catch (MailSendException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}

package com.sd365.digital.village.travel.platform.basedata.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class HotelAdminAccountQuery extends BaseQuery {
    /**
     * 酒店管理员账号
     */
    @ApiModelProperty(value="account酒店管理员账号")
    private String account;

    /**
     * 酒店管理员名字
     */
    @ApiModelProperty(value="name酒店管理员名字")
    private String name;

    /**
     * 管理员所属酒店
     */
    @ApiModelProperty(value="hotelId管理员所属酒店")
    private Long hotelId;

    /**
     * 所属供应商id
     */
    @ApiModelProperty(value="所属供应商id")
    private Long supplierId;
}

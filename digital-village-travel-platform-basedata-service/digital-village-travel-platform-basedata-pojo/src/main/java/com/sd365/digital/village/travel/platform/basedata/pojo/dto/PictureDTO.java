package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PictureDTO extends TenantBaseDTO {

    /**
     * 景点、酒店或者其他需要关联图片的表的主键
     */
    @ApiModelProperty(value="fkId景点、酒店或者其他需要关联图片的表的主键")
    private Long fkId;

    /**
     * 图片的url
     */
    @ApiModelProperty(value="url图片的url")
    private String url;
}

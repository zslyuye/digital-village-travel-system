package com.sd365.digital.village.travel.platform.basedata.pojo.vo;

import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value="com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionAdminAccountVO")
@Data
@AllArgsConstructor
public class AttractionAdminAccountVO extends TenantBaseVO {
    /**
     * 景点管理员账号
     */
    @ApiModelProperty(value="account景点管理员账号")
    private String account;

    /**
     * 景点管理员密码
     */
    @ApiModelProperty(value="password景点管理员密码")
    private String password;

    /**
     * 景点管理员名字
     */
    @ApiModelProperty(value="name景点管理员名字")
    private String name;

    /**
     * 景点管理员联系电话
     */
    @ApiModelProperty(value="phone景点管理员联系电话")
    private String phone;

    /**
     * 所属景点
     */
    @ApiModelProperty(value="attractionId所属景点")
    private Long attractionId;

    /**
     * 所属景点
     */
    @ApiModelProperty(value="所属景点")
    private AttractionDTO attractionDTO;

    /**
     * 所属供应商
     */
    @ApiModelProperty(value="所属供应商")
    private SupplierDTO supplierDTO;

    public AttractionAdminAccountVO(){
        attractionDTO = new AttractionDTO();
        supplierDTO =new SupplierDTO();
    }
}
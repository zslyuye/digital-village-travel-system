package com.sd365.digital.village.travel.platform.basedata.pojo.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class HotelRoomForPlanQuery {

    /**
     * 计划id
     */
    @ApiModelProperty(value = "planId计划id")
    private Long planId;

    /**
     * 酒店id
     */
    @ApiModelProperty(value = "hotelId酒店id")
    private Long hotelId;

    /**
     * 房型ID（默认双人床）
     */
    @ApiModelProperty(value = "roomId房型ID（默认双人床）")
    private Long roomId;

}

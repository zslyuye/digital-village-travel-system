package com.sd365.digital.village.travel.platform.basedata.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class AttractionAdminAccountQuery extends BaseQuery {
    /**
     * 景点管理员账号
     */
    @ApiModelProperty(value="account景点管理员账号")
    private String account;

    /**
     * 景点管理员名字
     */
    @ApiModelProperty(value="name景点管理员名字")
    private String name;

    /**
     * 所属景点
     */
    @ApiModelProperty(value="attractionId所属景点")
    private Long attractionId;

    /**
     * 所属供应商id
     */
    @ApiModelProperty(value="所属供应商id")
    private Long supplierId;
}

package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HotelRoomDTO extends TenantBaseDTO {
    /**
     * 酒店ID
     */
    @ApiModelProperty(value="hotelId酒店ID")
    private Long hotelId;

    /**
     * 房间名称
     */
    @ApiModelProperty(value="name房间名称")
    private String name;

    /**
     * 房型
     */
    @ApiModelProperty(value="type房型")
    private String type;

    /**
     * 价格
     */
    @ApiModelProperty(value="price价格")
    private Float price;

    /**
     * 床位信息，如：一张1.5米双人床
     */
    @ApiModelProperty(value="bedInfo床位信息，如：一张1.5米双人床")
    private String bedInfo;

    /**
     * 价格有效期起始日期
     */
    @ApiModelProperty(value="validstart价格有效期起始日期")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(
            pattern = "yyyy-MM-dd",
            timezone = "GMT+8"
    )
    private Date validstart;

    /**
     * 价格有效期结束日期
     */
    @ApiModelProperty(value="validend价格有效期结束日期")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(
            pattern = "yyyy-MM-dd",
            timezone = "GMT+8"
    )
    private Date validend;

    /**
     * 房间数量
     */
    @ApiModelProperty(value="count房间数量")
    private Integer count;

    /**
     * 三餐（111表示三餐均有）
     */
    @ApiModelProperty(value="meals三餐（111表示三餐均有）")
    private Byte meals;

    /**
     * 是否可修改（退款）
     */
    @ApiModelProperty(value="modifiable是否可修改（退款）")
    private Boolean modifiable;

    /**
     * wifi
     */
    @ApiModelProperty(value="wifiwifi")
    private Boolean wifi;

    /**
     * 空调
     */
    @ApiModelProperty(value="airConditioning空调")
    private Boolean airConditioning;

    /**
     * 停车场
     */
    @ApiModelProperty(value="park停车场")
    private Boolean park;

    /**
     * 24小时热水
     */
    @ApiModelProperty(value="hotWater24小时热水")
    private Boolean hotWater;

    /**
     * 液晶电视
     */
    @ApiModelProperty(value="tv液晶电视")
    private Boolean tv;

    /**
     * 吹风机
     */
    @ApiModelProperty(value="hairDryer吹风机")
    private Boolean hairDryer;

    /**
     * 电热水壶
     */
    @ApiModelProperty(value="electricKettle电热水壶")
    private Boolean electricKettle;

    /**
     * 暖气
     */
    @ApiModelProperty(value="heating暖气")
    private Boolean heating;

    /**
     * 房间图片
     */
    @ApiModelProperty(value="房间图片")
    private List<PictureDTO> pictureDTOS;

    public List<PictureDTO> getPictureDTOS() {
        return pictureDTOS;
    }

    public void setPictureDTOS(List<PictureDTO> pictureDTOS) {
        this.pictureDTOS = pictureDTOS;
    }

    /**
     * 酒店ID
     */
    public Long getHotelId() {
        return hotelId;
    }

    /**
     * 酒店ID
     */
    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * 房间名称
     */
    public String getName() {
        return name;
    }

    /**
     * 房间名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 房型
     */
    public String getType() {
        return type;
    }

    /**
     * 房型
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 价格
     */
    public Float getPrice() {
        return price;
    }

    /**
     * 价格
     */
    public void setPrice(Float price) {
        this.price = price;
    }

    /**
     * 床位信息，如：一张1.5米双人床
     */
    public String getBedInfo() {
        return bedInfo;
    }

    /**
     * 床位信息，如：一张1.5米双人床
     */
    public void setBedInfo(String bedInfo) {
        this.bedInfo = bedInfo;
    }

    /**
     * 价格有效期起始日期
     */
    public Date getValidstart() {
        return validstart;
    }

    /**
     * 价格有效期起始日期
     */
    public void setValidstart(Date validstart) {
        this.validstart = validstart;
    }

    /**
     * 价格有效期结束日期
     */
    public Date getValidend() {
        return validend;
    }

    /**
     * 价格有效期结束日期
     */
    public void setValidend(Date validend) {
        this.validend = validend;
    }

    /**
     * 房间数量
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 房间数量
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 三餐（111表示三餐均有）
     */
    public Byte getMeals() {
        return meals;
    }

    /**
     * 三餐（111表示三餐均有）
     */
    public void setMeals(Byte meals) {
        this.meals = meals;
    }

    /**
     * 是否可修改（退款）
     */
    public Boolean getModifiable() {
        return modifiable;
    }

    /**
     * 是否可修改（退款）
     */
    public void setModifiable(Object modifiable) {
        if(modifiable instanceof String){
            this.modifiable = "1".equals(modifiable) ? true : false;
        } else if(modifiable instanceof Integer) {
            this.modifiable = modifiable.equals(1) ? true : false;
        }else {
            this.modifiable = (Boolean) modifiable;
        }
    }

    /**
     * wifi
     */
    public Boolean getWifi() {
        return wifi;
    }

    /**
     * wifi
     */
    public void setWifi(Object wifi) {
        if(wifi instanceof Integer){
            this.wifi = "1".equals(wifi) ? true : false;
        } else {
            this.wifi = (Boolean) wifi;
        }
    }

    /**
     * 空调
     */
    public Boolean getAirConditioning() {
        return airConditioning;
    }

    /**
     * 空调
     */
    public void setAirConditioning(Object airConditioning) {
        if(airConditioning instanceof Integer){
            this.airConditioning = "1".equals(airConditioning) ? true : false;
        } else {
            this.airConditioning = (Boolean) airConditioning;
        }
    }

    /**
     * 停车场
     */
    public Boolean getPark() {
        return park;
    }

    /**
     * 停车场
     */
    public void setPark(Object park) {
        if(park instanceof Integer){
            this.park = "1".equals(park) ? true : false;
        } else {
            this.park = (Boolean) park;
        }
    }

    /**
     * 24小时热水
     */
    public Boolean getHotWater() {
        return hotWater;
    }

    /**
     * 24小时热水
     */
    public void setHotWater(Object hotWater) {
        if(hotWater instanceof Integer){
            this.hotWater = "1".equals(hotWater) ? true : false;
        } else {
            this.hotWater = (Boolean) hotWater;
        }
    }

    /**
     * 液晶电视
     */
    public Boolean getTv() {
        return tv;
    }

    /**
     * 液晶电视
     */
    public void setTv(Object tv) {
        if(tv instanceof Integer){
            this.tv = "1".equals(tv) ? true : false;
        } else {
            this.tv = (Boolean) tv;
        }
    }

    /**
     * 吹风机
     */
    public Boolean getHairDryer() {
        return hairDryer;
    }

    /**
     * 吹风机
     */
    public void setHairDryer(Object  hairDryer) {
        if(hairDryer instanceof Integer){
            this.hairDryer = "1".equals(hairDryer) ? true : false;
        } else {
            this.hairDryer = (Boolean) hairDryer;
        }
    }

    /**
     * 电热水壶
     */
    public Boolean getElectricKettle() {
        return electricKettle;
    }

    /**
     * 电热水壶
     */
    public void setElectricKettle(Object electricKettle) {
        if(electricKettle instanceof Integer){
            this.electricKettle = "1".equals(electricKettle) ? true : false;
        } else {
            this.electricKettle = (Boolean) electricKettle;
        }
    }

    /**
     * 暖气
     */
    public Boolean getHeating() {
        return heating;
    }

    /**
     * 暖气
     */
    public void setHeating(Object heating) {
        if(heating instanceof Integer){
            this.heating = "1".equals(heating) ? true : false;
        } else {
            this.heating = (Boolean) heating;
        }
    }

    public HotelRoomDTO(){
        pictureDTOS = new ArrayList<>();
    }
}
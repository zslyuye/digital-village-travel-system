package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class HotelRoomAndPictureDTO {

    @ApiModelProperty(value = "房间信息")
    private HotelRoomDTO hotelRoomDTO;

    @ApiModelProperty(value = "房间图片地址")
    private String[] pictureUrls;
}

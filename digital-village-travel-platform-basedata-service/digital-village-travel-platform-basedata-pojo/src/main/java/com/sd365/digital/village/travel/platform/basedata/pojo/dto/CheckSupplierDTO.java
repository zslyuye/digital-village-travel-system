package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckSupplierDTO {
    /**
     * 供应商对象
     */
    @ApiModelProperty(value="供应商对象")
    private SupplierDTO supplierDTO;

    /**
     * 审核信息
     */
    @ApiModelProperty(value="审核信息")
    private String message;

}

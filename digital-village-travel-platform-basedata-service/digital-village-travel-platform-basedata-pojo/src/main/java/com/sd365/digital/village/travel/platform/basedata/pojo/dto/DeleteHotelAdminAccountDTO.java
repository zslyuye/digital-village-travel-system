package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteHotelAdminAccountDTO {
    private Long id;
    private Long version;
}

package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberDTO extends TenantBaseDTO {
    /**
     * 账号
     */
    @ApiModelProperty(value="account账号")
    private String account;

    /**
     * 密码
     */
    @ApiModelProperty(value="password密码")
    private String password;

    /**
     * 姓名
     */
    @ApiModelProperty(value="name姓名")
    private String name;

    /**
     * 昵称
     */
    @ApiModelProperty(value="nickName昵称")
    private String nickName;

    /**
     * 头像图片地址
     */
    @ApiModelProperty(value="avatarUrl头像图片地址")
    private String avatarUrl;

    /**
     * 联系电话
     */
    @ApiModelProperty(value="mobile联系电话")
    private Long mobile;

    /**
     * 性别（0：男，1：女）
     */
    @ApiModelProperty(value="sex性别（0：男，1：女）")
    private Byte sex;

    /**
     * 个人账户
     */
    @ApiModelProperty(value="bankAccount个人账户")
    private String bankAccount;
}

package com.sd365.digital.village.travel.platform.basedata.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class HotelQuery extends BaseQuery {

    /**
     * 酒店名称
     */
    @ApiModelProperty(value="name酒店名称")
    private String name;

    /**
     * 酒店分类（0经济型、1舒适性、2商务型）
     */
    @ApiModelProperty(value="type酒店分类")
    private Byte type;

    /**
     * 景点地址
     */
    @ApiModelProperty(value="address酒店地址")
    private String address;

    /**
     * 评分(输入1.0降序排序)
     */
    @ApiModelProperty(value="score评分")
    private Float score;

    /**
     * 酒店星级
     */
    @ApiModelProperty(value="star酒店星级")
    private Integer star;

    /**
     * 参考价格(输入1.0降序排序)
     */
    @ApiModelProperty(value="参考价格")
    private Float referencePrice;

    /**
     * 状态
     */
    @ApiModelProperty(value="status状态")
    private Byte status;

    /**
     * 景点管理员id
     */
    @ApiModelProperty(value="酒店供应商id")
    private Long hotelSupplierId;
}
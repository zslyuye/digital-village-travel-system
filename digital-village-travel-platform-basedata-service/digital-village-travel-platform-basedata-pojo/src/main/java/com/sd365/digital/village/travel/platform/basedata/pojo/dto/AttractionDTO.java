package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AttractionDTO extends TenantBaseDTO {

    /**
     * 景点供应商id
     */
    @ApiModelProperty(value="attractionSupplierId景点供应商id")
    private Long attractionSupplierId;

    /**
     * 景点名称
     */
    @ApiModelProperty(value="name景点名称")
    private String name;


    /**
     * 景点分类(0自然风景、1公园乐园、2人文历史)
     */
    @ApiModelProperty(value="type景点分类")
    private Byte type;

    /**
     * 评分
     */
    @ApiModelProperty(value="mark评分")
    private Float mark;

    /**
     * 等级
     */
    @ApiModelProperty(value="level等级")
    private String level;

    /**
     * 参考价格
     */
    @ApiModelProperty(value="参考价格")
    private Float referencePrice;

    /**
     * 景点地址
     */
    @ApiModelProperty(value="address景点地址")
    private String address;

    /**
     * 景点简介
     */
    @ApiModelProperty(value="introduction景点简介")
    private String introduction;

    /**
     * 纬度
     */
    @ApiModelProperty(value="latitude纬度")
    private Double latitude;

    /**
     * 经度
     */
    @ApiModelProperty(value="longutude经度")
    private Double longitude;

    /**
     * 景点开放时间
     */
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value="openTime景点开放时间")
    private Date openTime;

    /**
     * 景点关闭时间
     */
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value="endTime景点关闭时间")
    private Date endTime;

    /**
     * 景点负责人
     */
    @ApiModelProperty(value="boss景点负责人")
    private String boss;

    /**
     * 景点负责人手机号
     */
    @ApiModelProperty(value="tel景点负责人手机号")
    private String tel;

    /**
     * 景点收益账户
     */
    @ApiModelProperty(value="bankAccount景点收益账户")
    private String bankAccount;

    /**
     * 景点收益总额
     */
    @ApiModelProperty(value="profitSum景点收益总额")
    private Float profitSum;

    /**
     * 顾客总数
     */
    @ApiModelProperty(value="customerSum顾客总数")
    private Integer customerSum;

    /**
     * 未处理订单总数
     */
    @ApiModelProperty(value="unsolveOrder未处理订单总数")
    private Integer unsolveOrder;

    /**
     * 在售门票种类
     */
    @ApiModelProperty(value="ticketTypeSum在售门票种类")
    private Integer ticketTypeSum;

    /**
     * 景点所属供应商
     */
    @Transient
    @ApiModelProperty(value="景点所属供应商")
    private SupplierDTO supplierDTO;

    /**
     * 景点图片
     */
    @Transient
    @ApiModelProperty(value="景点图片")
    private List<PictureDTO> pictureDTOS;

    public AttractionDTO(){
        supplierDTO =new SupplierDTO();
        pictureDTOS = new ArrayList<>();
    }
}

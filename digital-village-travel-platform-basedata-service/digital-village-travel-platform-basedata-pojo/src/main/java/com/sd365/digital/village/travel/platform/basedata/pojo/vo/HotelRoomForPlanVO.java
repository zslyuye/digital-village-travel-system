package com.sd365.digital.village.travel.platform.basedata.pojo.vo;

import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;



@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelRoomForPlanVO")
@Table(name = "hotel_room_for_plan")
public class HotelRoomForPlanVO extends TenantBaseVO {
    /**
     * 计划id
     */
    @ApiModelProperty(value = "planId计划id")
    private Long planId;

    /**
     * 酒店id
     */
    @ApiModelProperty(value = "hotelId酒店id")
    private Long hotelId;

    /**
     * 房型ID（默认双人床）
     */
    @ApiModelProperty(value = "roomId房型ID（默认双人床）")
    private Long roomId;

    /**
     * 价格
     */
    @ApiModelProperty(value = "price价格")
    private Float price;

    /**
     * 有效时间区段
     */
    @ApiModelProperty(value = "section有效时间区段")
    private Date section;
}

package com.sd365.digital.village.travel.platform.basedata.pojo.query;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberQuery extends TenantBaseQuery {
    /**
     * 昵称
     */
    @ApiModelProperty(value="nickName昵称")
    private String nickName;

    /**
     * 账号
     */
    @ApiModelProperty(value="account账号")
    private String account;

    /**
     * 姓名
     */
    @ApiModelProperty(value="name姓名")
    private String name;

    /**
     * 性别（0：男，1：女）
     */
    @ApiModelProperty(value="sex性别（0：男，1：女）")
    private Byte sex;
    /**
     * 状态
     */
    @ApiModelProperty(value="状态")
    private Byte status;

    /**
     * 个人账户
     */
    @ApiModelProperty(value="bankAccount个人账户")
    private String bankAccount;
}

package com.sd365.digital.village.travel.platform.basedata.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "图片查询条件对象：PictureQuery")
public class PictureQuery extends BaseQuery {

    /**
     * 景点、酒店或者其他需要关联图片的表的主键
     */
    private Long fkId;
}

package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddTemporaryDTO {
    private Long fkId;
    private HotelDTO hotelDTO;
    private AttractionDTO attractionDTO;
    private SupplierDTO supplierDTO;
    private List<String> imagesUrlList;
}

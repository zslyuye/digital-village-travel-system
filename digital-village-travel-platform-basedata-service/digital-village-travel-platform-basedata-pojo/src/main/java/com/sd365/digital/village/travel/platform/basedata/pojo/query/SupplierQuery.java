package com.sd365.digital.village.travel.platform.basedata.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "供应商查询条件对象：SupplierQuery")
public class SupplierQuery extends BaseQuery {
    /**
     * 供应商名称
     */
    @ApiModelProperty(value="name供应商名称")
    private String name;

    /**
     * 供应商账号
     */
    @ApiModelProperty(value="code供应商账号")
    private String code;

    /**
     * 供应商类型
     */
    @ApiModelProperty(value="type供应商类型")
    private Byte type;

    /**
     * 供应商状态
     */
    @ApiModelProperty(value="type供应商状态")
    private Byte status;
}

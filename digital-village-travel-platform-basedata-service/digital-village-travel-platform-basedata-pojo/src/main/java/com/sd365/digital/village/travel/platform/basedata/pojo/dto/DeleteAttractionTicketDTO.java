package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value="批量删除门票对象：DeleteAttractionTicketDTO")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteAttractionTicketDTO extends TenantBaseDTO {
    private Long id;
    private Long version;
}

package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SupplierDTO extends TenantBaseDTO {

    /**
     * 供应商类型
     */
    @ApiModelProperty(value="type供应商类型")
    private Byte type;

    /**
     * 供应商名称
     */
    @ApiModelProperty(value="name供应商名称")
    private String name;

    /**
     * 供应商帐号
     */
    @ApiModelProperty(value="code供应商帐号")
    private String code;

    /**
     * 密码
     */
    @ApiModelProperty(value="password密码")
    private String password;

    /**
     * 负责人
     */
    @ApiModelProperty(value="boss负责人")
    private String boss;

    /**
     * 联系方式
     */
    @ApiModelProperty(value="tel联系方式")
    private String tel;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    private String email;

    /**
     * 联系地址
     */
    @ApiModelProperty(value="address联系地址")
    private String address;

    /**
     * 营业执照图片
     */
    @Transient
    @ApiModelProperty(value="营业执照图片")
    private List<PictureDTO> pictureDTOS;

    public SupplierDTO(){
        this.pictureDTOS = new ArrayList<>();
    }

}

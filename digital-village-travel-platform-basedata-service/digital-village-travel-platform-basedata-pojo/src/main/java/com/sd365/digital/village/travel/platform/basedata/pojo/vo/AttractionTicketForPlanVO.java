package com.sd365.digital.village.travel.platform.basedata.pojo.vo;


import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionTicketForPlanVO")
@Table(name = "attraction_ticket_for_plan")
public class AttractionTicketForPlanVO extends TenantBaseVO {
    /**
     * 景点id
     */
    @ApiModelProperty(value = "attractionId景点id")
    private Long attractionId;

    /**
     * 计划id
     */
    @ApiModelProperty(value = "planId计划id")
    private Long planId;

    /**
     * 门票id
     */
    @ApiModelProperty(value = "ticketId门票id")
    private Long ticketId;

    /**
     * 门票分类(0成人、1学生、2儿童)
     */
    @ApiModelProperty(value = "type门票分类")
    private Byte type;

    /**
     * 优惠价格
     */
    @ApiModelProperty(value = "price优惠价格")
    private Float price;


}
package com.sd365.digital.village.travel.platform.basedata.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class AttractionQuery extends BaseQuery {

    /**
     * 景点名称
     */
    @ApiModelProperty(value="name景点名称")
    private String name;

    /**
     * 景点分类(0自然风景、1公园乐园、2人文历史)
     */
    @ApiModelProperty(value="type景点分类")
    private Byte type;

    /**
     * 景点地址
     */
    @ApiModelProperty(value="address景点地址")
    private String address;

    /**
     * 评分(输入1.0降序排序)
     */
    @ApiModelProperty(value="mark评分")
    private Float mark;

    /**
     * 等级（输入DESC降序排序）
     */
    @ApiModelProperty(value="level等级")
    private String level;

    /**
     * 参考价格(输入1.0降序排序)
     */
    @ApiModelProperty(value="参考价格")
    private Float referencePrice;

    /**
     * 景点管理员id
     */
    @ApiModelProperty(value="景点管理员id")
     private Long attractionSupplierId;

    /**
     * 状态
     */
    @ApiModelProperty(value="status状态")
    private Byte status;
}

package com.sd365.digital.village.travel.platform.basedata.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value="com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelAdminAccountDTO")
@Data
@AllArgsConstructor
public class HotelAdminAccountDTO extends TenantBaseDTO {
    /**
     * 酒店管理员账号
     */
    @ApiModelProperty(value="account酒店管理员账号")
    private String account;

    /**
     * 酒店管理员密码
     */
    @ApiModelProperty(value="password酒店管理员密码")
    private String password;

    /**
     * 酒店管理员名字
     */
    @ApiModelProperty(value="name酒店管理员名字")
    private String name;

    /**
     * 酒店管理员联系方式
     */
    @ApiModelProperty(value="phone酒店管理员联系方式")
    private String phone;

    /**
     * 管理员所属酒店
     */
    @ApiModelProperty(value="hotelId管理员所属酒店")
    private Long hotelId;

    /**
     * 管理员所属酒店
     */
    @ApiModelProperty(value="管理员所属酒店")
    private HotelDTO hotelDTO;

    /**
     * 所属供应商
     */
    @ApiModelProperty(value="所属供应商")
    private SupplierDTO supplierDTO;

    public HotelAdminAccountDTO(){
        hotelDTO = new HotelDTO();
        supplierDTO = new SupplierDTO();
    }
}
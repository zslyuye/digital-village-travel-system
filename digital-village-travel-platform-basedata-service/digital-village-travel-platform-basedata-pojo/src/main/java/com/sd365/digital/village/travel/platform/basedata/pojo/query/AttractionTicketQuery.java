package com.sd365.digital.village.travel.platform.basedata.pojo.query;


import com.sd365.common.core.common.pojo.vo.BaseQuery;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "门票查询条件对象：AttractionTicketQuery")
public class AttractionTicketQuery extends BaseQuery {
    /**
     * 景点id
     */
    private Long attractionId;
    /**
     * 门票名称
     */
    private String name;
    /**
     * 状态
     */
    private byte status;
}

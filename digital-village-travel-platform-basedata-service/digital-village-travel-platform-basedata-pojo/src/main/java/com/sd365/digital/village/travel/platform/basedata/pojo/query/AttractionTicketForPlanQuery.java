package com.sd365.digital.village.travel.platform.basedata.pojo.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttractionTicketForPlanQuery {
    /**
     * 景点id
     */
    @ApiModelProperty(value = "attractionId景点id")
    private Long attractionId;

    /**
     * 计划id
     */
    @ApiModelProperty(value = "planId计划id")
    private Long planId;

    /**
     * 门票id
     */
    @ApiModelProperty(value = "ticketId门票id")
    private Long ticketId;

    /**
     * 门票分类(0成人、1学生、2儿童)
     */
    @ApiModelProperty(value = "type门票分类")
    private Byte type;
}

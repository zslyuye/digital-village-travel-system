package com.sd365.digital.village.travel.platform.basedata.pojo.vo;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Transient;

@Data
@AllArgsConstructor
@ApiModel(value="门票展示对象：AttractionTicketVO")
public class AttractionTicketVO extends TenantBaseEntity {
    /**
     * 景点id
     */
    @ApiModelProperty(value="景点id")
    private Long attractionId;

    /**
     * 价格
     */
    @ApiModelProperty(value="价格")
    private Float price;

    /**
     * 销量
     */
    @ApiModelProperty(value="销量")
    private Integer saleNum;

    /**
     * 门票名称
     */
    @ApiModelProperty(value="门票名称")
    private String name;
    /**
     * 门票信息
     */
    @ApiModelProperty(value="name门票信息描述")
    private String info;
    /**
     * 所属景点
     */
    @Transient
    @ApiModelProperty(value = "所属景点")
    private AttractionDTO attractionDTO;

    public AttractionTicketVO() {
        this.attractionDTO = new AttractionDTO();
    }
}

package com.sd365.digital.village.travel.platform.basedata.pojo.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;


import javax.persistence.Transient;


@Data
@AllArgsConstructor
@ApiModel(value = "景点门票DTO对象：AttractionTicketDTO")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AttractionTicketDTO extends TenantBaseDTO {
    /**
     * 景点id
     */
    @ApiModelProperty(value="景点id")
    private Long attractionId;

    /**
     * 价格
     */
    @ApiModelProperty(value="价格")
    private Float price;

    /**
     * 销量
     */
    @ApiModelProperty(value="销量")
    private Integer saleNum;

    /**
     * 门票名称
     */
    @ApiModelProperty(value="门票名称")
    private String name;

    /**
     * 门票信息
     */
    @ApiModelProperty(value="name门票信息描述")
    private String info;
    /**
     * 所属景点
     */
    @Transient
    @ApiModelProperty(value = "所属景点")
    private AttractionDTO attractionDTO;

    public AttractionTicketDTO() {
        this.attractionDTO = new AttractionDTO();
    }

}

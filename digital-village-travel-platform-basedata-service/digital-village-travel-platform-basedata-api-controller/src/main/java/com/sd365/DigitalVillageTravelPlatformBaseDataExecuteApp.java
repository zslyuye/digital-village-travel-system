package com.sd365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@ComponentScans({
        @ComponentScan("com.sd365.common"),
        @ComponentScan("com.sd365.common.api.version")
})
@MapperScan("com.sd365.digital.village.travel.platform.basedata.dao.mapper")
public class DigitalVillageTravelPlatformBaseDataExecuteApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(DigitalVillageTravelPlatformBaseDataExecuteApp.class, args);
    }
}

package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.HotelApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AddTemporaryDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelVO;
import com.sd365.digital.village.travel.platform.basedata.service.HotelService;
import com.sd365.digital.village.travel.platform.basedata.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
public class HotelController extends AbstractController implements HotelApi {

    @Autowired
    private HotelService hotelService;
    @Autowired
    private PictureService pictureService;

    @Override
    @ApiLog
    @Transactional
    public Boolean add(@Valid AddTemporaryDTO addTemporaryDTO) {

        //添加酒店，获取添加后酒店id，失败则返回-1
        Long fkId= hotelService.add(addTemporaryDTO.getHotelDTO());
        if( fkId != -1 ) {
            //循环遍历前端传过来的图片Url列表，将图片信息插入到picture表中
            for ( String url : addTemporaryDTO.getImagesUrlList() ) {
                PictureDTO pictureDTO = new PictureDTO();
                pictureDTO.setFkId(fkId);
                pictureDTO.setUrl(url);
                pictureService.add(pictureDTO);
            }
            return true;
        }else{
            return  false;
        }
    }

    @Override
    @ApiLog
    public Boolean remove(Long id, Long version) {
        return hotelService.remove(id,version);
    }

    @Override
    @ApiLog
    public Boolean modify(@Valid HotelDTO hotelDTO) {
        return hotelService.modify(hotelDTO);
    }

    @Override
    @ApiLog
    public List<HotelVO> commonQuery(HotelQuery hotelQuery) {
        if(null==hotelQuery){
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID,new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<HotelDTO> hotelDTOS= hotelService.commonQuery(hotelQuery);
        List<HotelVO> hotelVOS= BeanUtil.copyList(hotelDTOS, HotelVO.class);
        return hotelVOS;
    }

    @ApiLog
    @Override
    public List<HotelVO> pcCommonQuery(HotelQuery hotelQuery) {
        if(null==hotelQuery){
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID,new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<HotelDTO> hotelDTOS= hotelService.pcCommonQuery(hotelQuery);
        List<HotelVO> hotelVOS= BeanUtil.copyList(hotelDTOS, HotelVO.class);
        return hotelVOS;
    }

    @Override
    @ApiLog
    public HotelVO queryHotelById(Long id) {
        HotelDTO hotelDTO =hotelService.queryHotelById(id);
        if (hotelDTO !=null){
            HotelVO hotelVO = BeanUtil.copy(hotelDTO,HotelVO.class);
            return hotelVO;
        }else {
            return null;
        }
    }

    @Override
    @ApiLog
    public Boolean batchUpdateHotel(@Valid HotelDTO[] hotelDTOS) {
        return hotelService.batchUpdateHotel(hotelDTOS);
    }

    @Override
    public Boolean modifyHotel(@Valid Long id, @Valid Integer unSolveOrder, @Valid Integer customerSum, @Valid Float profitSum) {
        return hotelService.modifyHotel(id,unSolveOrder,customerSum,profitSum);
    }
}

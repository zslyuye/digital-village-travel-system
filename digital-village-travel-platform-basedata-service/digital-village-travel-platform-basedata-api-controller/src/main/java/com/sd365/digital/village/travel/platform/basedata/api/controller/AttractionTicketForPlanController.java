package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.AttractionTicketForPlanApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionTicketForPlanDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionTicketForPlanQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionTicketForPlanVO;
import com.sd365.digital.village.travel.platform.basedata.service.AttractionTicketForPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
public class AttractionTicketForPlanController extends AbstractController implements AttractionTicketForPlanApi {

    @Autowired
    private AttractionTicketForPlanService attractionTicketForPlanService;

    @Override
    @ApiLog
    public Boolean add(@Valid AttractionTicketForPlanDTO attractionTicketForPlanDTO) {
        return attractionTicketForPlanService.add(attractionTicketForPlanDTO);
    }

    @Override
    public Boolean batchAdd(@Valid AttractionTicketForPlanDTO[] attractionTicketForPlanDTOS) {
        return attractionTicketForPlanService.batchAdd(attractionTicketForPlanDTOS);
    }

    @Override
    @ApiLog
    public List<AttractionTicketForPlanVO> commonQuery(AttractionTicketForPlanQuery attractionTicketForPlanQuery) {

        List<AttractionTicketForPlanDTO> attractionTicketForPlanDTOS = attractionTicketForPlanService.commonQuery(attractionTicketForPlanQuery);
        List<AttractionTicketForPlanVO> attractionTicketForPlanVOS = BeanUtil.copyList(attractionTicketForPlanDTOS, AttractionTicketForPlanVO.class);
        return attractionTicketForPlanVOS;
    }
}

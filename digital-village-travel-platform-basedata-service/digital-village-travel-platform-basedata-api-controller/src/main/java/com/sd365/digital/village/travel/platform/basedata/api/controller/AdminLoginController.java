package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import io.swagger.annotations.Api;
import lombok.Data;
import org.springframework.web.bind.annotation.*;


@RestController
@Api(tags = "管理员登录 ", value = "/digital/village/travel/system/platform/basedata/v1/admin")
@CrossOrigin
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/admin")
public class AdminLoginController {
    @ApiLog
    @CrossOrigin
    @PostMapping(value = "/login")
    @ResponseBody
    public UserVO login() {
        UserVO userVO = new UserVO();
        userVO.data = new ResponseData();
        userVO.data.setToken(1);
        userVO.setCode(20000);
        userVO.setMsg("登录成功");
        return userVO;
    }

    @ApiLog
    @CrossOrigin
    @GetMapping(value = "/info")
    @ResponseBody
    public UserVO info() {
        UserVO userVO = new UserVO();
        userVO.data = new ResponseData();
        userVO.data.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        userVO.data.setRoles("admin");
        userVO.data.setName("Super admin");
        userVO.setCode(2000);
        userVO.setMsg("登录成功");
        return userVO;
    }

    @ApiLog
    @CrossOrigin
    @PostMapping(value = "/logout")
    @ResponseBody
    public Boolean logout() {
        return true;
    }

    @Data
    static
    class ResponseData {
        private Integer token;
        private String roles;
        private String name;
        private String avatar;
    }

    @Data
    public static class UserVO {
        private String msg;
        private Integer code;
        private ResponseData data;
    }
}

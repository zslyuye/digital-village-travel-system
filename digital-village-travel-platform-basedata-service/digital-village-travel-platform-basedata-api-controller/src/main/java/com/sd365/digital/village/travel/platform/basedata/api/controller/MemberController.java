package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.MemberApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.MemberDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.MemberQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.MemberVO;
import com.sd365.digital.village.travel.platform.basedata.service.MemberService;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;


@RestController
public class MemberController extends AbstractController implements MemberApi {
    @Resource
    private MemberService memberService;

    @Override
    @ApiLog
    public Boolean add(@Valid MemberDTO memberDTO) {
        return memberService.add(memberDTO);
    }

    @Override
    @ApiLog
    public Boolean delete(Long id, Long version) {
        if (id == null || version == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("会员删除错误：条件非法"));
        }
        return memberService.delete(id, version);
    }

    @Override
    @ApiLog
    public List<MemberVO> commonQuery(MemberQuery memberQuery) {
        if (memberQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("会员查询错误：条件为null"));
        }
        List<MemberDTO> memberDTOList = memberService.commonQuery(memberQuery);
        return BeanUtil.copyList(memberDTOList, MemberVO.class);
    }

    @Override
    @ApiLog
    public MemberVO queryByAccount(String account) {
        if (account == null || "".equals(account)) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("会员账号查询错误：条件非法"));
        }
        MemberDTO memberDTO = memberService.queryByAccount(account);
        return BeanUtil.copy(memberDTO, MemberVO.class);
    }

    @Override
    @ApiLog
    public MemberVO queryById(Long id) {
        if (id == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("会员id查询错误：条件为null"));
        }
        MemberDTO memberDTO = memberService.queryById(id);
        return BeanUtil.copy(memberDTO, MemberVO.class);
    }

    @Override
    @ApiLog
    public Boolean update(@Valid MemberDTO memberDTO) {
        return memberService.update(memberDTO);
    }
}

package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.AttractionApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AddTemporaryDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionVO;
import com.sd365.digital.village.travel.platform.basedata.service.AttractionService;
import com.sd365.digital.village.travel.platform.basedata.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
public class AttractionController extends AbstractController implements AttractionApi {

    @Autowired
    private AttractionService attractionService;
    @Autowired
    private PictureService pictureService;

    @ApiLog
    @Override
    @Transactional
    public Boolean add(@Valid AddTemporaryDTO addTemporaryDTO) {

        //添加景点，获取添加后景点id，失败则返回-1
        Long fkId= attractionService.add(addTemporaryDTO.getAttractionDTO());
        if( fkId != -1 ) {
            //循环遍历前端传过来的图片Url列表，将图片信息插入到picture表中
            for ( String url : addTemporaryDTO.getImagesUrlList() ) {
                PictureDTO pictureDTO = new PictureDTO();
                pictureDTO.setFkId(fkId);
                pictureDTO.setUrl(url);
                pictureService.add(pictureDTO);
            }
            return true;
        }else{
            return  false;
        }
    }

    @Override
    @ApiLog
    public Boolean remove(Long id, Long version) {
        return attractionService.remove(id,version);
    }

    @Override
    @ApiLog
    public Boolean modify(@Valid AttractionDTO attractionDTO) {
        return attractionService.modify(attractionDTO);
    }

    @Override
    @ApiLog
    public List<AttractionVO> commonQuery(AttractionQuery attractionQuery) {
        if( null == attractionQuery){
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID,new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<AttractionDTO> attractionDTOS= attractionService.commonQuery(attractionQuery);
        List<AttractionVO> attractionVOS= BeanUtil.copyList(attractionDTOS, AttractionVO.class);
        return attractionVOS;
    }

    @Override
    @ApiLog
    public List<AttractionVO> pcCommonQuery(AttractionQuery attractionQuery) {
        if( null == attractionQuery){
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID,new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<AttractionDTO> attractionDTOS= attractionService.pcCommonQuery(attractionQuery);
        List<AttractionVO> attractionVOS= BeanUtil.copyList(attractionDTOS, AttractionVO.class);
        return attractionVOS;
    }

    @Override
    @ApiLog
    public AttractionVO queryAttractionById(Long id) {
        AttractionDTO attractionDTO =attractionService.queryAttractionById(id);
        if (attractionDTO !=null){
            AttractionVO attractionVO = BeanUtil.copy(attractionDTO,AttractionVO.class);
            return attractionVO;
        }else {
            return null;
        }
    }

    @Override
    @ApiLog
    public Boolean batchUpdateAttraction(@Valid AttractionDTO[] attractionDTOS) {
        return attractionService.batchUpdateAttraction(attractionDTOS);
    }

    @ApiLog
    @Override
    public Boolean modifyAttraction(@Valid Long id, @Valid Integer unSolveOrder, @Valid Integer customerSum, @Valid Float profitSum) {
        return attractionService.modifyAttraction(id,unSolveOrder,customerSum,profitSum);
    }
}

package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionAdminAccountVO;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelAdminAccountVO;
import com.sd365.digital.village.travel.platform.basedata.service.AttractionAdminAccountService;
import com.sd365.digital.village.travel.platform.basedata.service.HotelAdminAccountService;
import com.sd365.digital.village.travel.platform.basedata.service.SupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@RestController
@Api(tags = "供应商和旗下管理员登陆 ", value = "/digital/village/travel/system/platform/basedata/v1/supplier")
@CrossOrigin
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/supplier")
public class LoginController extends AbstractController {
    @Resource
    private HotelAdminAccountService hotelAdminAccountService;
    @Resource
    private AttractionAdminAccountService attractionAdminAccountService;
    @Resource
    private SupplierService supplierService;

    /**
     * 会话对象属性名
     **/
    public static final String LOGGED_USER = "loggedUser";
    /**
     * 酒店管理员类型码
     **/
    public static final Byte HOTEL_ADMIN_CODE = 1;
    /**
     * 景点管理员类型码
     **/
    public static final Byte ATTRACTION_ADMIN_CODE = 0;

    /**
     * 供应商账号登录验证控制器
     **/
    @PostMapping("login")
    @ApiLog
    @ApiOperation(tags = "供应商登录", value = "login")
    public SupplierVO supplierLogin(@RequestBody FormData formData) {
        // 查询账号信息
        SupplierDTO supplierDTO = supplierService.queryByAccount(formData.getAccount());
        if (supplierDTO != null) {
            // 账号状态判断
            if (supplierDTO.getStatus() == -1) {
                SupplierVO supplierVO = new SupplierVO();
                supplierVO.setCode(0);
                supplierVO.setMsg("请耐心等待审核");
                return supplierVO;
            }
            if (supplierDTO.getStatus() == 0) {
                SupplierVO supplierVO = new SupplierVO();
                supplierVO.setCode(0);
                supplierVO.setMsg("账号未通过审核");
                return supplierVO;
            }
            if (supplierDTO.getStatus() == 2) {
                SupplierVO supplierVO = new SupplierVO();
                supplierVO.setCode(0);
                supplierVO.setMsg("账号已被封禁");
                return supplierVO;
            }
            // 验证账号和密码
            if (supplierDTO.getPassword().equals(formData.getPassword())) {
                SupplierVO supplierVO = new SupplierVO();
                supplierVO.data.setToken(1);
                supplierVO.data.setSupplierDTO(supplierDTO);
                supplierVO.setCode(20000);
                supplierVO.setMsg("登录成功");
                return supplierVO;
            }
        }
        // 验证失败
        SupplierVO supplierVO = new SupplierVO();
        supplierVO.setCode(0);
        supplierVO.setMsg("用户名或密码错误");
        return supplierVO;
    }

    /**
     * 获取当前登录的供应商信息
     **/
    @GetMapping("info")
    @ApiLog
    @ApiOperation(tags = "获取当前登录的供应商信息", value = "info")
    public SupplierVO supplierInfo(@RequestParam("token") String token, @RequestParam("account") String account) {
        SupplierVO supplierVO = new SupplierVO();
        // 是否查询到了账号
        if (account != null) {
            // 查询账号信息
            SupplierDTO supplierDTO = supplierService.queryByAccount(account);
            if (supplierDTO != null) {
                supplierVO.data.setSupplierDTO(supplierDTO);
                supplierVO.data.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
                supplierVO.data.setRoles("admin");
                supplierVO.data.setName("管理员");
                supplierVO.data.setToken(1);
                supplierVO.setCode(2000);
                supplierVO.setMsg("获取登录用户信息成功");
                return supplierVO;
            }
        }
        // 登录失败
        supplierVO.setCode(0);
        supplierVO.setMsg("获取登录用户信息失败");
        return supplierVO;
    }

    /**
     * 供应商登出
     **/
    @PostMapping("logout")
    @ApiLog
    @ApiOperation(tags = "登出", value = "logout")
    public Boolean supplierLogout() {
        return true;
    }

    /**
     * 供应商旗下管理员登录验证
     **/
    @PostMapping("admin/login")
    @ApiLog
    @ApiOperation(tags = "供应商旗下管理员登录", value = "admin/login")
    public UserVO adminLogin(@RequestBody @Valid FormData formData, HttpSession httpSession) {
        if (formData.getType().equals(HOTEL_ADMIN_CODE)) {
            // 酒店管理员
            HotelAdminAccountDTO hotelAdminAccountDTO = hotelAdminAccountService.queryByAccount(formData.getAccount());
            if (hotelAdminAccountDTO != null) {
                // 账号被停用
                if (hotelAdminAccountDTO.getStatus() == 0) {
                    UserVO userVO = new UserVO();
                    userVO.setCode(0);
                    userVO.setMsg("账号已被停用");
                    return userVO;
                }
                // 验证账号和密码
                if (hotelAdminAccountDTO.getPassword().equals(formData.getPassword())) {
                    UserVO userVO = new UserVO();
                    userVO.data.setToken(1);
                    userVO.setCode(20000);
                    userVO.setMsg("登录成功");
                    // 将管理员账号信息返回给前端
                    userVO.data.setHotelAdminAccountVO(BeanUtil.copy(hotelAdminAccountDTO, HotelAdminAccountVO.class));
                    httpSession.setAttribute(LOGGED_USER, userVO);
                    return userVO;
                }
            }
        }
        if (formData.getType().equals(ATTRACTION_ADMIN_CODE)) {
            // 景点管理员
            AttractionAdminAccountDTO attractionAdminAccountDTO = attractionAdminAccountService.queryByAccount(formData.getAccount());
            if (attractionAdminAccountDTO != null) {
                // 账号被停用
                if (attractionAdminAccountDTO.getStatus() == 0) {
                    UserVO userVO = new UserVO();
                    userVO.setCode(0);
                    userVO.setMsg("账号已被停用");
                    return userVO;
                }
                // 验证账号和密码
                if (attractionAdminAccountDTO.getPassword().equals(formData.getPassword())) {
                    UserVO userVO = new UserVO();
                    userVO.data.setToken(1);
                    userVO.setCode(20000);
                    userVO.setMsg("登录成功");
                    // 将管理员账号信息返回给前端
                    userVO.data.setAttractionAdminAccountVO(BeanUtil.copy(attractionAdminAccountDTO, AttractionAdminAccountVO.class));
                    httpSession.setAttribute(LOGGED_USER, userVO);
                    return userVO;
                }
            }
        }
        // 验证失败
        UserVO userVO = new UserVO();
        userVO.setCode(0);
        userVO.setMsg("账号或密码错误");
        return userVO;
    }

    /**
     * 获取当前会话管理员信息
     **/
    @GetMapping("admin/info")
    @ApiLog
    @ApiOperation(tags = "获取当前会话的管理员信息", value = "admin/info")
    public UserVO adminInfo(HttpSession httpSession) {
        Object obj = httpSession.getAttribute(LOGGED_USER);
        if (obj == null) {
            UserVO userVO = new UserVO();
            userVO.setCode(0);
            userVO.setMsg("当前没有会话对象");
            return userVO;
        }
        return (UserVO) obj;
    }

    /**
     * 管理员账号信息
     **/
    @Data
    static class AddminAccountData {
        private Integer token;
        private AttractionAdminAccountVO attractionAdminAccountVO;
        private HotelAdminAccountVO hotelAdminAccountVO;

        public AddminAccountData() {
            attractionAdminAccountVO = new AttractionAdminAccountVO();
            hotelAdminAccountVO = new HotelAdminAccountVO();
        }
    }

    /**
     * 用户信息类
     **/
    @Data
    public static class UserVO {
        private String msg;
        private Integer code;
        private AddminAccountData data;

        public UserVO() {
            data = new AddminAccountData();
        }
    }

    /**
     * 接受前端登录表单数据
     **/
    @Data
    public static class FormData {
        private String account;
        private String password;
        private Byte type;
    }

    /**
     * 供应商账号信息
     **/
    @Data
    static class SupplierAccountData {
        private Integer token;
        private String roles;
        private String name;
        private String avatar;
        private SupplierDTO supplierDTO;

        public SupplierAccountData() {
            this.supplierDTO = new SupplierDTO();
        }
    }

    /**
     * 用户信息类
     **/
    @Data
    public static class SupplierVO {
        private String msg;
        private Integer code;
        private SupplierAccountData data;

        public SupplierVO() {
            data = new SupplierAccountData();
        }
    }
}

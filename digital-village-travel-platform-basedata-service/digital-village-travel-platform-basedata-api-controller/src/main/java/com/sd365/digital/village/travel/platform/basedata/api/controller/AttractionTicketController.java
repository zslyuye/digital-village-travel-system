package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.AttractionTicketApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionTicketDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteAttractionTicketDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionTicketQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionTicketVO;
import com.sd365.digital.village.travel.platform.basedata.service.AttractionTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AttractionTicketController extends AbstractController implements AttractionTicketApi {


    @Autowired
    private AttractionTicketService attractionTicketService;

    @ApiLog
    @Override
    public List<AttractionTicketVO> selectAttractionTicket(AttractionTicketQuery attractionTicketQuery) {
        List<AttractionTicketDTO> attractionTicketDTOS = attractionTicketService.commonQuery(attractionTicketQuery);
        List<AttractionTicketVO> attractionTicketVOS = BeanUtil.copyList(attractionTicketDTOS, AttractionTicketVO.class);
        return attractionTicketVOS;
    }

    @ApiLog
    @Override
    public Boolean modify(@Valid AttractionTicketDTO attractionTicketDTO) {
        return attractionTicketService.modify(attractionTicketDTO);
    }

    @ApiLog
    @Override
    public Boolean add(AttractionTicketDTO attractionTicketDTO) {
        return attractionTicketService.add(attractionTicketDTO);
    }

    @ApiLog
    @Override
    public AttractionTicketVO queryAttractionTicketById(Long id) {
        AttractionTicketDTO attractionTicketDTO = attractionTicketService.queryById(id);
        if(attractionTicketDTO != null){
            AttractionTicketVO attractionTicketVO=BeanUtil.copy(attractionTicketDTO,AttractionTicketVO.class);
            return attractionTicketVO;
        }else{
            return null;
        }
    }

    @ApiLog
    @Override
    public Boolean remove(Long id, Long version) {
        return attractionTicketService.remove(id,version);
    }

    @ApiLog
    @Override
    public Boolean batchRemove(@Valid DeleteAttractionTicketDTO[] deleteAttractionTicketDTOS) {
        return attractionTicketService.batchRemove(deleteAttractionTicketDTOS);
    }
}

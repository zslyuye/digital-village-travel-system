package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.AttractionAdminAccountApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteAttractionAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionAdminAccountQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionAdminAccountVO;
import com.sd365.digital.village.travel.platform.basedata.service.AttractionAdminAccountService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;


@RestController
public class AttractionAdminAccountController extends AbstractController implements AttractionAdminAccountApi {
    @Resource
    private AttractionAdminAccountService attractionAdminAccountService;

    @Override
    @ApiLog
    public Boolean add(@RequestBody @Valid AttractionAdminAccountDTO attractionAdminAccountDTO) {
        return attractionAdminAccountService.add(attractionAdminAccountDTO);
    }

    @Override
    @ApiLog
    public Boolean delete(Long id, Long version) {
        return attractionAdminAccountService.delete(id, version);
    }

    @Override
    @ApiLog
    public List<AttractionAdminAccountVO> commonQuery(AttractionAdminAccountQuery attractionAdminAccountQuery) {
        if (attractionAdminAccountQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("景点管理员查询错误：查询条件为null"));
        }
        List<AttractionAdminAccountDTO> attractionAdminAccountDTOList = attractionAdminAccountService.commonQuery(attractionAdminAccountQuery);
        return BeanUtil.copyList(attractionAdminAccountDTOList, AttractionAdminAccountVO.class);
    }

    @Override
    @ApiLog
    public Boolean update(@RequestBody @Valid AttractionAdminAccountDTO attractionAdminAccountDTO) {
        return attractionAdminAccountService.update(attractionAdminAccountDTO);
    }

    @Override
    @ApiLog
    public AttractionAdminAccountVO queryByAccount(String account) {
        if (account == null || "".equals(account)) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("景点管理员账户查询错误：查询条件为null"));
        }
        AttractionAdminAccountDTO attractionAdminAccountDTO = attractionAdminAccountService.queryByAccount(account);
        if (attractionAdminAccountDTO == null) {
            return null;
        }
        return BeanUtil.copy(attractionAdminAccountDTO, AttractionAdminAccountVO.class);
    }

    @Override
    @ApiLog
    public Boolean batchRemoveAttractionAdminAccount(@Valid DeleteAttractionAdminAccountDTO[] deleteAttractionAdminAccountDTOS) {
        return attractionAdminAccountService.batchRemoveAttractionAdminAccount(deleteAttractionAdminAccountDTOS);
    }

    @Override
    @ApiLog
    public Boolean batchUpdateAttractionAdminAccount(@Valid AttractionAdminAccountDTO[] attractionAdminAccountDTOS) {
        return attractionAdminAccountService.batchUpdateAttractionAdminAccount(attractionAdminAccountDTOS);
    }
}

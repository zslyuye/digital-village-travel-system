package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.MemberDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.MemberVO;
import com.sd365.digital.village.travel.platform.basedata.service.MemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@Api(tags = "会员登陆 ", value = "/digital/village/travel/system/member/v1/")
@CrossOrigin
@RestController
@RequestMapping(value = "/digital/village/travel/system/member/v1/")
public class MemberLoginController {
    @Resource
    private MemberService memberService;

    /**
     * 会话对象属性名
     **/
    public static final String LOGGED_USER = "loggedUser";

    /**
     * 会员注册
     **/
    @PostMapping("register")
    @ApiLog
    @ApiOperation(tags = "会员注册", value = "register")
    public Boolean register(@RequestBody MemberDTO memberDTO) {
        return memberService.add(memberDTO);
    }

    /**
     * 会员登录验证
     **/
    @PostMapping("login")
    @ApiLog
    @ApiOperation(tags = "会员登陆验证", value = "login")
    public UserVO login(@RequestBody @Valid FormData formData) {
        // 按照账户查询用户
        MemberDTO memberDTO = memberService.queryByAccount(formData.getAccount());
        if (memberDTO != null) {
            // 账号状态判断
            if (memberDTO.getStatus() == 0) {
                UserVO userVO = new UserVO();
                userVO.setCode(0);
                userVO.setMsg("账号已被封禁");
                return userVO;
            }
            // 验证账号和密码
            if (memberDTO.getPassword().equals(formData.getPassword())) {
                UserVO userVO = new UserVO();
                userVO.data.setToken(1);
                userVO.setCode(20000);
                userVO.setMsg("登录成功");
                // 将会员账号信息返回给前端
                userVO.data.setMemberVO(BeanUtil.copy(memberDTO, MemberVO.class));
                return userVO;
            }
        }
        // 验证失败
        UserVO userVO = new UserVO();
        userVO.setCode(0);
        userVO.setMsg("登录失败");
        return userVO;
    }

    /**
     * 获取当前会话会员信息
     **/
    @GetMapping("info")
    @ApiLog
    @ApiOperation(tags = "当前会话的会员信息获取", value = "info")
    public UserVO info(HttpSession httpSession) {
        Object obj = httpSession.getAttribute(LOGGED_USER);
        if (obj == null) {
            UserVO userVO = new UserVO();
            userVO.setCode(0);
            userVO.setMsg("当前没有会话对象");
            return userVO;
        }
        return (UserVO) obj;
    }

    /**
     * response
     **/
    @Data
    static class ResponseData {
        private Integer token;
        private MemberVO memberVO;

        public ResponseData() {
            memberVO = new MemberVO();
        }
    }

    /**
     * 用户信息类
     **/
    @Data
    public static class UserVO {
        private String msg;
        private Integer code;
        private ResponseData data;

        public UserVO() {
            data = new ResponseData();
        }
    }

    /**
     * 接受前端登录表单数据
     **/
    @Data
    public static class FormData {
        private String account;
        private String password;
    }
}

package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.SupplierApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AddTemporaryDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.CheckSupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.SupplierQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.SupplierVO;
import com.sd365.digital.village.travel.platform.basedata.service.PictureService;
import com.sd365.digital.village.travel.platform.basedata.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class SupplierController implements SupplierApi {

    @Autowired
    private SupplierService supplierService;
    @Autowired
    private PictureService pictureService;

    @ApiLog
    @Override
    @Transactional
    public Boolean add(@Valid AddTemporaryDTO addTemporaryDTO) {

        //添加供应商，获取添加后供应商id，失败则返回-1
        Long fkId= supplierService.add(addTemporaryDTO.getSupplierDTO());
        if( fkId != -1 ) {
            //循环遍历前端传过来的图片Url列表，将图片信息插入到picture表中
            for ( String url : addTemporaryDTO.getImagesUrlList() ) {
                PictureDTO pictureDTO = new PictureDTO();
                pictureDTO.setFkId(fkId);
                pictureDTO.setUrl(url);
                pictureService.add(pictureDTO);
            }
            return true;
        }else{
            return  false;
        }
    }

    @ApiLog
    @Override
    public Boolean remove(Long id, Long version) {
        return supplierService.remove(id,version);
    }

    @ApiLog
    @Override
    public Boolean modify(@Valid SupplierDTO supplierDTO) {
        return supplierService.modify(supplierDTO);
    }

    @Override
    @ApiLog
    public Boolean check(@Valid CheckSupplierDTO checkSupplierDTO) {
        return supplierService.check(checkSupplierDTO);
    }

    @ApiLog
    @Override
    public List<SupplierVO> commonQuery(SupplierQuery supplierQuery) {
        if( null == supplierQuery){
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID,new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<SupplierDTO> supplierDTOS= supplierService.commonQuery(supplierQuery);
        List<SupplierVO> supplierVOS= BeanUtil.copyList(supplierDTOS, SupplierVO.class);
        return supplierVOS;
    }

    @ApiLog
    @Override
    public SupplierVO queryById(Long id) {
        SupplierDTO supplierDTO = supplierService.queryById(id);
        if (supplierDTO != null) {
            SupplierVO supplierVO = BeanUtil.copy(supplierDTO, SupplierVO.class);
            return supplierVO;
        } else {
            return null;
        }
    }

    @Override
    public SupplierVO queryByAccount(String account) {
        SupplierDTO supplierDTO = supplierService.queryByAccount(account);
        if(supplierDTO == null){
            return null;
        }
        return BeanUtil.copy(supplierDTO, SupplierVO.class);
    }
}

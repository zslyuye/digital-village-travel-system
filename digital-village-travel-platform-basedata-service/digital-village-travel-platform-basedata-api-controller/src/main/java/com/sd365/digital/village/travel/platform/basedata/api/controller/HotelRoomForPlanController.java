package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.HotelRoomForPlanApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomForPlanDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelRoomForPlanQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelRoomForPlanVO;
import com.sd365.digital.village.travel.platform.basedata.service.HotelRoomForPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;



@RestController
public class HotelRoomForPlanController extends AbstractController implements HotelRoomForPlanApi {

    @Autowired
    private HotelRoomForPlanService hotelRoomForPlanService;

    @Override
    @ApiLog
    public Boolean add(@Valid HotelRoomForPlanDTO hotelRoomForPlanDTO) {
        return hotelRoomForPlanService.add(hotelRoomForPlanDTO);
    }

    @Override
    @ApiLog
    public List<HotelRoomForPlanVO> commonQuery(HotelRoomForPlanQuery hotelRoomForPlanQuery) {

        List<HotelRoomForPlanDTO> hotelRoomForPlanDTOS = hotelRoomForPlanService.commonQuery(hotelRoomForPlanQuery);
        List<HotelRoomForPlanVO> hotelRoomForPlanVOS = BeanUtil.copyList(hotelRoomForPlanDTOS, HotelRoomForPlanVO.class);
        return hotelRoomForPlanVOS;
    }
}

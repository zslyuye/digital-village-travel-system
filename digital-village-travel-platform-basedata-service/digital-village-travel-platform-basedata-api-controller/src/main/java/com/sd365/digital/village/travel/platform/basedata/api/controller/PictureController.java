package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.PictureApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AddTemporaryDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeletePictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.PictureQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.PictureVO;
import com.sd365.digital.village.travel.platform.basedata.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
public class PictureController extends AbstractController implements PictureApi {

    @Autowired
    private PictureService pictureService;

    @Override
    @ApiLog
    public List<String> fileUpload(@RequestParam("file") MultipartFile file) {
        List<String> pictureUrlList = new ArrayList<>();
        if (!file.isEmpty()){
            pictureUrlList.add(pictureService.fileUpload(file));
           return pictureUrlList;
        }
        return pictureUrlList;
    }

    @Override
    @ApiLog
    public List<String> fileListUpload(@RequestParam("files") List<MultipartFile> fileList) {
        if (fileList.size()>0){
            return pictureService.fileListUpload(fileList);
        }
        return null;
    }

    @ApiLog
    @Override
    public List<PictureVO> selectPicture(PictureQuery pictureQuery) {
        List<PictureDTO> pictureDTOS = pictureService.commonQuery(pictureQuery);
        List<PictureVO> pictureVOS = BeanUtil.copyList(pictureDTOS, PictureVO.class);
        return pictureVOS;
    }

    @ApiLog
    @Override
    public Boolean modify(@Valid PictureDTO pictureDTO) {
        return pictureService.modify(pictureDTO);
    }

    @ApiLog
    @Override
    public Boolean add(@Valid PictureDTO pictureDTO) {
        return pictureService.add(pictureDTO);
    }

    @ApiLog
    @Override
    @Transactional
    public Boolean batchAdd(@Valid AddTemporaryDTO addTemporaryDTO) {
        for(String url : addTemporaryDTO.getImagesUrlList()) {
            PictureDTO pictureDTO = new PictureDTO();
            pictureDTO.setFkId(addTemporaryDTO.getFkId());
            pictureDTO.setUrl(url);
            pictureService.add(pictureDTO);
        }
        return true;
    }

    @ApiLog
    @Override
    public PictureVO queryPictureById(Long id) {
        PictureDTO pictureDTO= pictureService.queryById(id);
        if(pictureDTO != null){
            PictureVO pictureVO = BeanUtil.copy(pictureDTO,PictureVO.class);
            return pictureVO;
        }else{
            return null;
        }
    }

    @ApiLog
    @Override
    public Boolean remove(Long id, Long version) {
        return pictureService.remove(id,version);
    }

    @ApiLog
    @Override
    public Boolean batchRemove(@Valid DeletePictureDTO[] deletePictureDTOS) {
        return pictureService.batchRemove(deletePictureDTOS);
    }
}

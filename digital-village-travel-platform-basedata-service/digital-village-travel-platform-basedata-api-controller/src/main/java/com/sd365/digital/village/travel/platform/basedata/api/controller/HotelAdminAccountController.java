package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.HotelAdminAccountApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteHotelAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelAdminAccountQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelAdminAccountVO;
import com.sd365.digital.village.travel.platform.basedata.service.HotelAdminAccountService;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;


@RestController
public class HotelAdminAccountController extends AbstractController implements HotelAdminAccountApi {
    @Resource
    private HotelAdminAccountService hotelAdminAccountService;

    @Override
    @ApiLog
    public Boolean add(@Valid HotelAdminAccountDTO hotelAdminAccountDTO) {
        return hotelAdminAccountService.add(hotelAdminAccountDTO);
    }

    @Override
    @ApiLog
    public Boolean delete(Long id, Long version) {
        return hotelAdminAccountService.delete(id, version);
    }

    @Override
    @ApiLog
    public List<HotelAdminAccountVO> commonQuery(HotelAdminAccountQuery hotelAdminAccountQuery) {
        if (hotelAdminAccountQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("酒店管理员查询错误：查询条件为null"));
        }
        List<HotelAdminAccountDTO> hotelAdminAccountDTOList = hotelAdminAccountService.commonQuery(hotelAdminAccountQuery);

        return BeanUtil.copyList(hotelAdminAccountDTOList, HotelAdminAccountVO.class);
    }

    @Override
    @ApiLog
    public Boolean update(@Valid HotelAdminAccountDTO hotelAdminAccountDTO) {
        return hotelAdminAccountService.update(hotelAdminAccountDTO);
    }

    @Override
    @ApiLog
    public HotelAdminAccountVO queryByAccount(String account) {
        if (account == null || "".equals(account)) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("景点管理员账户查询错误：查询条件为null"));
        }
        HotelAdminAccountDTO hotelAdminAccountDTO = hotelAdminAccountService.queryByAccount(account);
        if (hotelAdminAccountDTO == null) {
            return null;
        }
        return BeanUtil.copy(hotelAdminAccountDTO, HotelAdminAccountVO.class);
    }

    @Override
    @ApiLog
    public Boolean batchRemoveHotelAdminAccount(@Valid DeleteHotelAdminAccountDTO[] deleteHotelAdminAccountDTOS) {
        return hotelAdminAccountService.batchRemoveHotelAdminAccount(deleteHotelAdminAccountDTOS);
    }

    @Override
    @ApiLog
    public Boolean batchUpdateHotelAdminAccount(@Valid HotelAdminAccountDTO[] hotelAdminAccountDTOS) {
        return hotelAdminAccountService.batchUpdateHotelAdminAccount(hotelAdminAccountDTOS);
    }
}

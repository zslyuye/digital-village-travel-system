package com.sd365.digital.village.travel.platform.basedata.api.controller;

import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.api.HotelRoomApi;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomAndPictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelRoomQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelRoomVO;
import com.sd365.digital.village.travel.platform.basedata.service.HotelRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class HotelRoomController implements HotelRoomApi {

    @Autowired
    HotelRoomService hotelRoomService;

    @ApiLog
    @Override
    public Boolean addRoom(@Valid HotelRoomAndPictureDTO hotelRoomAndPictureDTO) {
        return hotelRoomService.addRoom(hotelRoomAndPictureDTO);
    }

    @Override
    public Boolean modifyRoom(@Valid HotelRoomDTO hotelRoomDTO) {
        return hotelRoomService.modifyRoom(hotelRoomDTO);
    }

    @Override
    public Boolean decreaseRoom(@Valid Long hotelId, @Valid Long id, @Valid Integer count) {
        return hotelRoomService.decreaseRoom(hotelId, id, count);
    }

    @Override
    public Boolean deleteRoom(@Valid HotelRoomDTO hotelRoomDTO) {
        return hotelRoomService.deleteRoom(hotelRoomDTO);
    }

    @Override
    @ApiLog
    public List<HotelRoomVO> queryRoom(HotelRoomQuery hotelRoomQuery) {
        if(hotelRoomQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID,new Exception("commonQuery 方法错误"));
        }

        List<HotelRoomDTO> hotelRoomDTOS = hotelRoomService.queryRoom(hotelRoomQuery);
        List<HotelRoomVO> hoteRoomVOS = BeanUtil.copyList(hotelRoomDTOS, HotelRoomVO.class);
        return hoteRoomVOS;
    }


}

package com.sd365.digital.village.travel.platform.basedata.service.impl;

import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.AttractionTicketMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.AttractionTicket;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionTicketDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteAttractionTicketDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionTicketQuery;
import com.sd365.digital.village.travel.platform.basedata.service.AttractionTicketService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import tk.mybatis.mapper.entity.Example;

import javax.validation.Valid;
import java.util.List;

@Service
public class AttractionTicketServiceImpl implements AttractionTicketService {

    @Autowired
    private AttractionTicketMapper attractionTicketMapper;

    @Autowired
    private IdGenerator idGenerator;

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Boolean add(@Valid AttractionTicketDTO attractionTicketDTO) {
        AttractionTicket attractionTicket= BeanUtil.copy(attractionTicketDTO,AttractionTicket.class);
        attractionTicket.setId(idGenerator.snowflakeId());
        return attractionTicketMapper.insert(attractionTicket)>0 ;
    }

    @Override
    public Boolean remove(Long id, Long version) {
        Example example=new Example(AttractionTicket.class);
        example.createCriteria().andEqualTo("id",id).andEqualTo("version",version);
        return attractionTicketMapper.deleteByExample(example)>0;
    }

    @Override
    public Boolean batchRemove(DeleteAttractionTicketDTO[] deleteAttractionTicketDTOS) {
        Assert.notEmpty(deleteAttractionTicketDTOS,"批量删除参数不可以为空");
        for(DeleteAttractionTicketDTO deleteAttractionTicketDTO : deleteAttractionTicketDTOS){
            Example example=new Example(AttractionTicket.class);
            example.createCriteria().andEqualTo("id",deleteAttractionTicketDTO.getId()).andEqualTo("version",deleteAttractionTicketDTO.getVersion()) ;
            int result= attractionTicketMapper.deleteByExample(example);
            Assert.isTrue(result>0,String.format("删除的记录id %d 没有匹配到版本",deleteAttractionTicketDTO.getId()));
        }
        return true;
    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public Boolean modify(AttractionTicketDTO attractionTicketDTO) {
        try {
            AttractionTicket attractionTicket = BeanUtil.copy(attractionTicketDTO, AttractionTicket.class);
            Example example=new Example(AttractionTicket.class);
            example.createCriteria().andEqualTo("id",attractionTicketDTO.getId())
                    .andEqualTo("version", attractionTicketDTO.getVersion());
            return attractionTicketMapper.updateByExampleSelective(attractionTicket,example)>0 ;

        }catch (BeanException ex){
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION,ex);
        } catch (Exception ex){
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE,ex);

        }
    }

    @Override
    public List<AttractionTicketDTO> commonQuery(AttractionTicketQuery attractionTicketQuery) {
        if(attractionTicketQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID,new Exception("commonQuery 方法错误"));
        }
        try {

            AttractionTicket attractionTicket = new AttractionTicket();
            attractionTicket.setName(attractionTicketQuery.getName());
            attractionTicket.setStatus(attractionTicketQuery.getStatus());
            attractionTicket.setAttractionId(attractionTicketQuery.getAttractionId());
            List<AttractionTicket> attractionTickets= attractionTicketMapper.commonQuery(attractionTicket);
            List<AttractionTicketDTO> attractionTicketDTOS= BeanUtil.copyList(attractionTickets, AttractionTicketDTO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    // 调用 BeanUtil.copyProperties
                    if(o==null || o1==null)
                        throw new BeanException("拷贝对象不可以为空",new Exception("copyList 拷贝回调错误"));

                    AttractionTicket attractionTicketSource=(AttractionTicket) o;
                    AttractionTicketDTO attractionTicketDTO=(AttractionTicketDTO)o1;
                    BeanUtil.copy(attractionTicketSource.getAttraction(),attractionTicketDTO.getAttractionDTO(), AttractionDTO.class);


                }
            });

            return attractionTicketDTOS;
        } catch (BeanException ex){
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION,ex);
        } catch (Exception ex){
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE,ex);

        }
    }

    @Override
    public AttractionTicketDTO queryById(Long id) {
        try{
            AttractionTicketDTO attractionTicketDTO = null;
            AttractionTicket attractionTicket = attractionTicketMapper.selectById(id);
            if(attractionTicket != null) {
                attractionTicketDTO = BeanUtil.copy(attractionTicket, AttractionTicketDTO.class);
                BeanUtil.copy(attractionTicket.getAttraction(),attractionTicketDTO.getAttractionDTO(), AttractionDTO.class);
            }
            return attractionTicketDTO;
        }catch (BeanException ex){
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION,ex);
        } catch (Exception ex){
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE,ex);
        }
    }
}

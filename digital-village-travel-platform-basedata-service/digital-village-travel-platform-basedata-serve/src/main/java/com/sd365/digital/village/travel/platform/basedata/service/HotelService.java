package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelQuery;
import java.util.List;


public interface HotelService {

    /**
     * 添加酒店
     * @param hotelDTO
     * @return
     */
    Long add(HotelDTO hotelDTO);


    /**
     * 删除酒店
     * @param id
     * @param version
     * @return
     */
    Boolean remove(Long id,Long version);

    /**
     * 修改酒店信息
     * @param hotelDTO
     * @return
     */

    Boolean modify(HotelDTO hotelDTO);


    /**
     * 根据条件查询酒店信息
     * @param hotelQuery
     * @return
     */
    List<HotelDTO> commonQuery(HotelQuery hotelQuery);

    /**
     * pc端根据条件查询酒店信息
     * @param hotelQuery
     * @return
     */
    List<HotelDTO> pcCommonQuery(HotelQuery hotelQuery);

    /**
     * 查询酒店通过ID
     * @param id
     * @return
     */
    HotelDTO queryHotelById(Long id);

    /**
     * 批量修改酒店
     * @param hotelDTOS
     * @return
     */
    Boolean batchUpdateHotel(HotelDTO[] hotelDTOS);

    /**
     * 修改酒店的营业信息
     * @param id  酒店id
     * @param unSolveOrder 酒店未处理订单
     * @param customerSum 酒店顾客数量
     * @param profitSum 酒店收益总额
     * @return
     */
    Boolean modifyHotel(Long id,Integer unSolveOrder,Integer customerSum,Float profitSum);
}

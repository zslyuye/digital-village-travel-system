package com.sd365.digital.village.travel.platform.basedata.service;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionQuery;

import java.util.List;


public interface AttractionService {

    /**
     * 添加景点信息
     * @param attractionDTO
     * @return
     */
    Long add(AttractionDTO attractionDTO);

    /**
     * 修改景点信息
     * @param attractionDTO
     * @return
     */
    Boolean modify(AttractionDTO attractionDTO);


    /**
     * 根据条件查询景点信息
     * @param attractionQuery
     * @return
     * */
    List<AttractionDTO> commonQuery(AttractionQuery attractionQuery);

    /**
     * pc端根据条件查询景点信息
     * @param attractionQuery
     * @return
     * */
    List<AttractionDTO> pcCommonQuery(AttractionQuery attractionQuery);


    /**
     * 查询景点通过ID
     * @param id
     * @return
     */
    AttractionDTO queryAttractionById(Long id);

    /**
     * 移除景点
     *
     * @param id      要移除的景点id
     * @param version api版本
     * @return java.lang.Boolean 是否移除成果
     **/
    Boolean remove(Long id, Long version);

    /**
     * 批量修改景点
     * @param attractionDTOS
     * @return
     */
    Boolean batchUpdateAttraction(AttractionDTO[] attractionDTOS);

    /**
     * 修改景点的营业信息
     * @param id  景点id
     * @param unSolveOrder 景点未处理订单
     * @param customerSum 景点顾客数量
     * @param profitSum 景点收益总额
     */
    Boolean modifyAttraction(Long id,Integer unSolveOrder,Integer customerSum,Float profitSum);
}

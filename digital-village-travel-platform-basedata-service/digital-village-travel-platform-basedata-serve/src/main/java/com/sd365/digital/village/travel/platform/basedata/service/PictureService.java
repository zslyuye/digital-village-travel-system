package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeletePictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.PictureQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface PictureService {

    /**
     * 上传单个图片并返回url
     * @param file
     * @return
     */
    String fileUpload(MultipartFile file);

    /**
     * 上传多个图片并返回url
     * @param fileList
     * @return
     */
    List<String> fileListUpload(List<MultipartFile> fileList);

    /**
     * 添加新的展示图片
     *
     * @param pictureDTO 新的图片DTO对象
     * @return java.lang.Boolean 是否添加成功
     **/
    Boolean add(PictureDTO pictureDTO);

    /**
     * 移除图片
     *
     * @param id      要移除的图片id
     * @param version api版本
     * @return java.lang.Boolean 是否移除成果
     **/
    Boolean remove(Long id, Long version);

    /**
     * 批量删除图片
     *
     * @param deletePictureDTOS 要删除的图片数组
     * @return java.lang.Boolean 删除结果
     **/
    @Transactional(rollbackFor = Exception.class)
    Boolean batchRemove(DeletePictureDTO[] deletePictureDTOS);

    /**
     * 修改图片信息
     *
     * @param pictureDTO 图片DTO对象
     * @return java.lang.Boolean 是否修改成功
     **/
    Boolean modify(PictureDTO pictureDTO);

    /**
     * 通用查询图片
     *
     * @param pictureQuery 查询条件
     * @return java.util.List<PictureDTO> 查询结果集
     **/
    List<PictureDTO> commonQuery(PictureQuery pictureQuery);

    /**
     * @param: 图片id
     * @return: PictureDTO
     * @see
     * @since
     */
    PictureDTO queryById(Long id);

}

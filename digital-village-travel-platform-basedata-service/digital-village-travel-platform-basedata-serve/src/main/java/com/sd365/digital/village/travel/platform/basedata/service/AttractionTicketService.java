package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionTicketDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteAttractionTicketDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionTicketQuery;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

public interface AttractionTicketService {
    /**
     * 添加门票种类
     *
     * @param attractionTicketDTO 新的门票DTO对象
     * @return java.lang.Boolean 是否添加成功
     **/
    Boolean add( AttractionTicketDTO attractionTicketDTO);

    /**
     * 移除门票种类
     *
     * @param id      要移除的门票种类id
     * @param version api版本
     * @return java.lang.Boolean 是否移除成果
     **/
    Boolean remove(Long id, Long version);

    /**
     * 批量删除门票种类
     *
     * @param deleteAttractionTicketDTOS 要删除的门票种类数组
     * @return java.lang.Boolean 删除结果
     **/
    @Transactional(rollbackFor = Exception.class)
    Boolean batchRemove(DeleteAttractionTicketDTO[] deleteAttractionTicketDTOS);

    /**
     * 修改门票信息
     *
     * @param attractionTicketDTO 门票DTO对象
     * @return java.lang.Boolean 是否修改成功
     **/
    Boolean modify(AttractionTicketDTO attractionTicketDTO);

    /**
     * 通用查询运单
     *
     * @param attractionTicketQuery 查询条件
     * @return java.util.List<AttractionTicketDTO> 查询结果集
     **/
    List<AttractionTicketDTO> commonQuery(AttractionTicketQuery attractionTicketQuery);

    /**
     * 根据id查询景点门票信息
     * @param id
     * @return
     */
    AttractionTicketDTO queryById(Long id);
}

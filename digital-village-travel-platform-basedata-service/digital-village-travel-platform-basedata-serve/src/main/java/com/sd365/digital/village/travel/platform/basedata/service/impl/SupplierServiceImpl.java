package com.sd365.digital.village.travel.platform.basedata.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.aop.MyPageInfo;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.SupplierMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.Supplier;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.CheckSupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.SupplierQuery;
import com.sd365.digital.village.travel.platform.basedata.service.SupplierService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import com.sd365.digital.village.travel.platform.basedata.utils.SendEmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private SendEmailUtil sendEmailUtil;

    @Autowired
    private IdGenerator idGenerator;


    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Long add(SupplierDTO supplierDTO) {
        Supplier supplier = BeanUtil.copy(supplierDTO, Supplier.class);
        supplier.setId(idGenerator.snowflakeId());
        if (supplierMapper.insert(supplier) > 0) {
            return supplier.getId();
        } else {
            return (long) -1;
        }
    }

    @Override
    public Boolean remove(Long id, Long version) {
        Example example = new Example(Supplier.class);
        example.createCriteria().andEqualTo("id", id).andEqualTo("version", version);
        return supplierMapper.deleteByExample(example) > 0;
    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public Boolean modify(SupplierDTO supplierDTO) {
        try {
            Supplier supplier = BeanUtil.copy(supplierDTO, Supplier.class);
            Example example = new Example(Supplier.class);
            example.createCriteria().andEqualTo("id", supplierDTO.getId())
                    .andEqualTo("version", supplierDTO.getVersion());
            return supplierMapper.updateByExample(supplier, example) > 0;

        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE, ex);

        }
    }

    @Override
    public Boolean check(CheckSupplierDTO checkSupplierDTO) {

        try {
            Supplier supplier = BeanUtil.copy(checkSupplierDTO.getSupplierDTO(), Supplier.class);
            Example example = new Example(Supplier.class);
            example.createCriteria().andEqualTo("id", checkSupplierDTO.getSupplierDTO().getId())
                    .andEqualTo("version", checkSupplierDTO.getSupplierDTO().getVersion());
            if (supplierMapper.updateByExample(supplier, example) > 0) {
                return sendEmailUtil.sendEmail(checkSupplierDTO.getSupplierDTO().getEmail(), checkSupplierDTO.getSupplierDTO().getStatus(), checkSupplierDTO.getMessage());
            } else {
                return false;
            }
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE, ex);

        }
    }

    @Override
    public List<SupplierDTO> commonQuery(SupplierQuery supplierQuery) {
        if (supplierQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("commonQuery 方法错误"));
        }
        try {
            Supplier supplier = new Supplier();
            supplier.setName(supplierQuery.getName());
            supplier.setCode(supplierQuery.getCode());
            supplier.setStatus(supplierQuery.getStatus());
            supplier.setType(supplierQuery.getType());
            List<Supplier> suppliers = supplierMapper.commonQuery(supplier);
            Page page = (Page) suppliers;
            BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
            //对象转化 po list to dto list
            List<SupplierDTO> supplierDTOS = BeanUtil.copyList(suppliers, SupplierDTO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    // 调用 BeanUtil.copyProperties
                    if (o == null || o1 == null) {
                        throw new BeanException("拷贝对象不可以为空", new Exception("copyList 拷贝回调错误"));
                    }

                    Supplier supplierSource = (Supplier) o;
                    SupplierDTO supplierDTO = (SupplierDTO) o1;
                    supplierDTO.setPictureDTOS(BeanUtil.copyList(supplierSource.getPictures(), PictureDTO.class));
                }
            });

            return supplierDTOS;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_ATTRACTION_DB_EXCEPTION_CODE, ex);

        }
    }

    @Override
    public SupplierDTO queryById(Long id) {
        try {
            SupplierDTO supplierDTO = null;
            Supplier supplier = supplierMapper.selectById(id);
            if (supplier != null) {
                supplierDTO = BeanUtil.copy(supplier, SupplierDTO.class);
                supplierDTO.setPictureDTOS(BeanUtil.copyList(supplier.getPictures(), PictureDTO.class));
            }
            return supplierDTO;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_ATTRACTION_DB_EXCEPTION_CODE, ex);
        }
    }

    @Override
    public SupplierDTO queryByAccount(String account) {
        if(account == null || "".equals(account)){
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID,new Exception("根据account查询供应商账号 参数为空"));
        }
        Example example = new Example(Supplier.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("code", account);
        List<Supplier> supplierList = supplierMapper.selectByExample(example);
        if(supplierList.isEmpty()){
            return null;
        }
        return BeanUtil.copy(supplierList.get(0), SupplierDTO.class);
    }
}

package com.sd365.digital.village.travel.platform.basedata.service.impl;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.aop.MyPageInfo;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.HotelAdminAccountMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.HotelAdminAccount;
import com.sd365.digital.village.travel.platform.basedata.entity.Supplier;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteHotelAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelAdminAccountQuery;
import com.sd365.digital.village.travel.platform.basedata.service.HotelAdminAccountService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Service
public class HotelAdminAccountServiceImpl implements HotelAdminAccountService {
    @Resource
    private HotelAdminAccountMapper hotelAdminAccountMapper;
    @Resource
    private IdGenerator idGenerator;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Boolean add(HotelAdminAccountDTO hotelAdminAccountDTO) {
        if (hotelAdminAccountDTO == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("酒店管理员添加：酒店管理员DTO对象为空"));
        }
        HotelAdminAccount hotelAdminAccount = BeanUtil.copy(hotelAdminAccountDTO, HotelAdminAccount.class);
        hotelAdminAccount.setId(idGenerator.snowflakeId());
        // 批量清除缓存
        long delete = batchDeleteCache("hotelAdminAccount_commonQuery");
        return hotelAdminAccountMapper.insert(hotelAdminAccount) > 0;
    }

    @Override
    public Boolean delete(Long id, Long version) {
        if (id == null || version == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("酒店管理员删除：id或version为null"));
        }
        Example example = new Example(HotelAdminAccount.class);
        example.createCriteria().andEqualTo("id", id)
                .andEqualTo("version", version);
        // 批量清除缓存
        long delete = batchDeleteCache("hotelAdminAccount_commonQuery");
        return hotelAdminAccountMapper.deleteByExample(example) > 0;
    }

    @Override
    public List<HotelAdminAccountDTO> commonQuery(HotelAdminAccountQuery hotelAdminAccountQuery) {
        // 对查询条件进行判空
        if (hotelAdminAccountQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("酒店管理员查询：查询条件非法"));
        }

        try {
            // 将查询条件转换为实体
            HotelAdminAccount hotelAdminAccount = new HotelAdminAccount();
            Supplier supplier = new Supplier();
            supplier.setId(hotelAdminAccountQuery.getSupplierId());
            hotelAdminAccount.setAccount(hotelAdminAccountQuery.getAccount());
            hotelAdminAccount.setName(hotelAdminAccountQuery.getName());
            hotelAdminAccount.setHotelId(hotelAdminAccountQuery.getHotelId());
            hotelAdminAccount.setSupplier(supplier);
            // 从redis缓存取出页数据
            String listStr = (String) redisTemplate.opsForValue().get("hotelAdminAccount_commonQuery" + "::" + hotelAdminAccountQuery.toString());
            List<HotelAdminAccount> hotelAdminAccountList = null;
            Page<HotelAdminAccount> page = null;
            if (listStr == null) {
                hotelAdminAccountList = hotelAdminAccountMapper.commonQuery(hotelAdminAccount);
                // 分页,使用Redis缓存后再使用分页，需要先清除缓存
                page = (Page<HotelAdminAccount>) hotelAdminAccountList;
                redisTemplate.opsForValue().set("hotelAdminAccount_commonQuery" + "::" + hotelAdminAccountQuery.toString(), JSON.toJSONString(hotelAdminAccountList));
                // 缓存PageInfo（分页信息）
                Map<String, Integer> pageInfo = new HashMap<>();
                pageInfo.put("pageNum", page.getPageNum());
                pageInfo.put("pages", page.getPages());
                pageInfo.put("total", (int) page.getTotal());
                redisTemplate.opsForValue().set("hotelAdminAccount_commonQuery_page_info" + "::" + hotelAdminAccountQuery.toString(), JSON.toJSONString(pageInfo));
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
            } else {
                hotelAdminAccountList = JSON.parseArray(listStr).toJavaList(HotelAdminAccount.class);
                Map<String, Integer> map = JSON.parseObject((String) redisTemplate.opsForValue().get("hotelAdminAccount_commonQuery_page_info" + "::" + hotelAdminAccountQuery.toString())).toJavaObject(Map.class);
                page = new Page<HotelAdminAccount>();
                for (HotelAdminAccount hotelAdminAccount1 : hotelAdminAccountList) {
                    page.add(hotelAdminAccount1);
                }
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo((long) map.get("total"), map.get("pages"))));
            }
            List<HotelAdminAccountDTO> hotelAdminAccountDTOS = BeanUtil.copyList(hotelAdminAccountList, HotelAdminAccountDTO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    // 调用 BeanUtil.copyProperties
                    if (o == null || o1 == null) {
                        throw new BeanException("拷贝对象不可以为空", new Exception("copyList 拷贝回调错误"));
                    }

                    HotelAdminAccount hotelAdminAccount1 = (HotelAdminAccount) o;
                    HotelAdminAccountDTO hotelAdminAccountDTO = (HotelAdminAccountDTO) o1;
                    BeanUtil.copy(hotelAdminAccount1.getHotel(), hotelAdminAccountDTO.getHotelDTO(), SupplierDTO.class);
                    BeanUtil.copy(hotelAdminAccount1.getSupplier(), hotelAdminAccountDTO.getSupplierDTO(), SupplierDTO.class);
                }
            });
            // 将查询所得实体结果列表转换为DTO对象列表
            return hotelAdminAccountDTOS;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_HOTEL_ADMIN_ACCOUNT_DB_EXCEPTION_CODE, ex);
        }

    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public Boolean update(HotelAdminAccountDTO hotelAdminAccountDTO) {
        if (hotelAdminAccountDTO == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("酒店管理员修改：参数为空"));
        }
        try {
            HotelAdminAccount hotelAdminAccount = BeanUtil.copy(hotelAdminAccountDTO, HotelAdminAccount.class);
            Example example = new Example(HotelAdminAccount.class);
            example.createCriteria().andEqualTo("id", hotelAdminAccount.getId())
                    .andEqualTo("version", hotelAdminAccount.getVersion());
            // 批量清除缓存
            long delete = batchDeleteCache("hotelAdminAccount_commonQuery");
            return hotelAdminAccountMapper.updateByExample(hotelAdminAccount, example) > 0;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_HOTEL_ADMIN_ACCOUNT_DB_EXCEPTION_CODE, ex);
        }
    }

    @Override
    public HotelAdminAccountDTO queryByAccount(String account) {
        HotelAdminAccount hotelAdminAccount = hotelAdminAccountMapper.queryByAccount(account);
        if (hotelAdminAccount == null) {
            return null;
        }
        return BeanUtil.copy(hotelAdminAccount, HotelAdminAccountDTO.class);
    }

    @Override
    public Boolean batchRemoveHotelAdminAccount(DeleteHotelAdminAccountDTO[] deleteHotelAdminAccountDTOS) {
        Assert.notEmpty(deleteHotelAdminAccountDTOS, "批量删除参数不可以为空");
        for (DeleteHotelAdminAccountDTO deleteHotelAdminAccountDTO : deleteHotelAdminAccountDTOS) {
            Example example = new Example(HotelAdminAccount.class);
            example.createCriteria().andEqualTo("id", deleteHotelAdminAccountDTO.getId()).andEqualTo("version", deleteHotelAdminAccountDTO.getVersion());
            int result = hotelAdminAccountMapper.deleteByExample(example);
            Assert.isTrue(result > 0, String.format("删除的记录id %d 没有匹配到版本", deleteHotelAdminAccountDTO.getId()));
        }
        // 批量清除缓存
        long delete = batchDeleteCache("hotelAdminAccount_commonQuery");
        return true;
    }

    @Override
    public Boolean batchUpdateHotelAdminAccount(HotelAdminAccountDTO[] hotelAdminAccountDTOS) {
        Assert.noNullElements(hotelAdminAccountDTOS, "更新数组不能为空");
        for (HotelAdminAccountDTO hotelAdminAccountDTO : hotelAdminAccountDTOS) {
            HotelAdminAccount hotelAdminAccount = BeanUtil.copy(hotelAdminAccountDTO, HotelAdminAccount.class);
            Example example = new Example(hotelAdminAccount.getClass());
            example.createCriteria().andEqualTo("id", hotelAdminAccount.getId()).andEqualTo("version", hotelAdminAccount.getVersion());
            int result = hotelAdminAccountMapper.updateByExampleSelective(hotelAdminAccount, example);
            Assert.isTrue(result > 0, "没有找到相应id更新记录");
        }
        // 批量清除缓存
        long delete = batchDeleteCache("hotelAdminAccount_commonQuery");
        return true;
    }

    /**
     * 根据key前缀批量删除缓存（缓存数据多的情况下性能差）
     *
     * @param key
     * @return
     */
    private Long batchDeleteCache(String key) {
        Set<String> keys = redisTemplate.keys(key + "*");
        return redisTemplate.delete(keys);
    }
}

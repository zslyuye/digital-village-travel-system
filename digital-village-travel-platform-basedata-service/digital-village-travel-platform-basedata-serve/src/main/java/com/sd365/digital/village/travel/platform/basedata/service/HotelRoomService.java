package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomAndPictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelRoomQuery;

import java.util.List;


public interface HotelRoomService {

    /**
     * 增加酒店的房间，其中必要的参数为所属酒店的id
     * @param hotelRoomAndPictureDTO
     * @return
     */
    Boolean addRoom(HotelRoomAndPictureDTO hotelRoomAndPictureDTO);

    /**
     * 修改酒店房间的信息，根据房间id和酒店hotelId，然后选择性地更改：updateByExampleSelective
     * @param hotelRoomDTO
     * @return
     */
    Boolean modifyRoom(HotelRoomDTO hotelRoomDTO);

    Boolean deleteRoom(HotelRoomDTO hotelRoomDTO);

    /**
     * 查询房型信息，其中查询条件可以为：hotelId, 区间[validstart, validend], status
     * @param hotelRoomQuery
     * @return
     */
    List<HotelRoomDTO> queryRoom(HotelRoomQuery hotelRoomQuery);

    /**
     * 对于某个酒店hotelId, 某个房型id，减少count的房间数量
     * @param hotelId
     * @param id
     * @param count
     * @return
     */
    Boolean decreaseRoom(Long hotelId, Long id, Integer count);
}

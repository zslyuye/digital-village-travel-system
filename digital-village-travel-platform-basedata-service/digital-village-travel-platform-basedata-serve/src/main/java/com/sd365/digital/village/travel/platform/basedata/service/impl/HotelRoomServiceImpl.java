package com.sd365.digital.village.travel.platform.basedata.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.aop.MyPageInfo;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.DaoException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.core.common.exception.code.DaoErrorCode;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.HotelRoomMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.HotelRoom;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomAndPictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelRoomQuery;
import com.sd365.digital.village.travel.platform.basedata.service.HotelRoomService;
import com.sd365.digital.village.travel.platform.basedata.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

// TODO 捕捉异常
@Service
public class HotelRoomServiceImpl implements HotelRoomService {

    @Autowired
    IdGenerator idGenerator;

    @Autowired
    private PictureService pictureService;

    @Autowired
    HotelRoomMapper hotelRoomMapper;

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Boolean addRoom(HotelRoomAndPictureDTO hotelRoomAndPictureDTO) {
        if(hotelRoomAndPictureDTO.getHotelRoomDTO().getHotelId() == null) {
            throw new DaoException(DaoErrorCode.FAIL_TO_CREATE, new Exception("缺少酒店id, 房型添加失败"));
        }
        // DTO cast to Entity
        HotelRoom hotelRoom = null;
        try {
            hotelRoom = BeanUtil.copy(hotelRoomAndPictureDTO.getHotelRoomDTO(), HotelRoom.class);
        } catch (BeanException e) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, e);
        } catch (Exception e) {
            e.printStackTrace();
        }

        hotelRoom.setId(idGenerator.snowflakeId());
        for (String url : hotelRoomAndPictureDTO.getPictureUrls()){
            PictureDTO pictureDTO = new PictureDTO();
            pictureDTO.setFkId(hotelRoom.getId());
            pictureDTO.setUrl(url);
            pictureService.add(pictureDTO);
        }
        return hotelRoomMapper.insert(hotelRoom) > 0;
    }

    @Override
    @Transactional
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public Boolean modifyRoom(HotelRoomDTO hotelRoomDTO) {
        try{
            HotelRoom hotelRoom = BeanUtil.copy(hotelRoomDTO, HotelRoom.class);
            Example example = new Example(HotelRoom.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("id", hotelRoomDTO.getId());
            criteria.andEqualTo("hotelId",hotelRoomDTO.getHotelId());
            return hotelRoomMapper.updateByExampleSelective(hotelRoom, example) > 0;
        }catch (BeanException ex){
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION,ex);
        }
    }

    @Override
    public Boolean deleteRoom(HotelRoomDTO hotelRoomDTO) {
        try {
            HotelRoom hotelRoom = BeanUtil.copy(hotelRoomDTO, HotelRoom.class);
        } catch (Exception e) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, e);
        }
        Example example = new Example(HotelRoom.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("id", hotelRoomDTO.getId());
        criteria.andEqualTo("hotelId",hotelRoomDTO.getHotelId());
        return hotelRoomMapper.deleteByExample(example) > 0;
    }

    @Override
    public List<HotelRoomDTO> queryRoom(HotelRoomQuery hotelRoomQuery) {
        HotelRoom hotelRoom = BeanUtil.copy(hotelRoomQuery, HotelRoom.class);
        List<HotelRoom> hotelRooms = hotelRoomMapper.selectHotelRoom(hotelRoom);
        Page page = (Page) hotelRooms;
        BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
        // PO list to DTO list
        List<HotelRoomDTO> HotelRoomDTOs = BeanUtil.copyList(hotelRooms, HotelRoomDTO.class, new BeanUtil.CopyCallback() {
            @Override
            public void copy(Object o, Object o1) {
                // 调用 BeanUtil.copyProperties
                if(o==null || o1==null) {
                    throw new BeanException("拷贝对象不可以为空",new Exception("copyList 拷贝回调错误"));
                }

                HotelRoom hotelRoom1 = (HotelRoom) o;
                HotelRoomDTO hotelRoomDTO = (HotelRoomDTO) o1;
                hotelRoomDTO.setPictureDTOS(BeanUtil.copyList(hotelRoom1.getPictures(), PictureDTO.class));
            }
        });
        return HotelRoomDTOs;
    }

    @Override
    public Boolean decreaseRoom(Long hotelId, Long id, Integer count) {
        return hotelRoomMapper.decreaseRoom(hotelId, id, count);
    }

}

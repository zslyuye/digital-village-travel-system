package com.sd365.digital.village.travel.platform.basedata.service.impl;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.aop.MyPageInfo;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.AttractionAdminAccountMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.AttractionAdminAccount;
import com.sd365.digital.village.travel.platform.basedata.entity.Supplier;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteAttractionAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionAdminAccountQuery;
import com.sd365.digital.village.travel.platform.basedata.service.AttractionAdminAccountService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Service
public class AttractionAdminAccountServiceImpl implements AttractionAdminAccountService {
    @Resource
    private AttractionAdminAccountMapper attractionAdminAccountMapper;
    @Resource
    private IdGenerator idGenerator;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Boolean add(AttractionAdminAccountDTO attractionAdminAccountDTO) {
        if (attractionAdminAccountDTO == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("景点管理员添加：景点管理员DTO对象为空"));
        }
        AttractionAdminAccount attractionAdminAccount = BeanUtil.copy(attractionAdminAccountDTO, AttractionAdminAccount.class);
        attractionAdminAccount.setId(idGenerator.snowflakeId());
        // 批量清除缓存
        long delete = batchDeleteCache("attractionAdminAccount_commonQuery");
        return attractionAdminAccountMapper.insert(attractionAdminAccount) > 0;
    }

    @Override
    public Boolean delete(Long id, Long version) {
        if (id == null || version == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("景点管理员删除：id或version为null"));
        }
        Example example = new Example(AttractionAdminAccount.class);
        example.createCriteria().andEqualTo("id", id)
                .andEqualTo("version", version);
        // 批量清除缓存
        long delete = batchDeleteCache("attractionAdminAccount_commonQuery");
        return attractionAdminAccountMapper.deleteByExample(example) > 0;
    }

    @Override
    public List<AttractionAdminAccountDTO> commonQuery(AttractionAdminAccountQuery attractionAdminAccountQuery) {
        // 对查询条件进行判空
        if (attractionAdminAccountQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("景点管理员查询：查询条件非法"));
        }

        try {
            // 将查询条件转换为实体
            AttractionAdminAccount attractionAdminAccount = new AttractionAdminAccount();
            Supplier supplier = new Supplier();
            supplier.setId(attractionAdminAccountQuery.getSupplierId());
            attractionAdminAccount.setAccount(attractionAdminAccountQuery.getAccount());
            attractionAdminAccount.setName(attractionAdminAccountQuery.getName());
            attractionAdminAccount.setAttractionId(attractionAdminAccountQuery.getAttractionId());
            attractionAdminAccount.setSupplier(supplier);
            // 从redis缓存取出页数据
            String listStr = (String) redisTemplate.opsForValue().get("attractionAdminAccount_commonQuery" + "::" + attractionAdminAccountQuery.toString());
            List<AttractionAdminAccount> attractionAdminAccountList = null;
            Page<AttractionAdminAccount> page = null;
            if (listStr == null) {
                attractionAdminAccountList = attractionAdminAccountMapper.commonQuery(attractionAdminAccount);
                // 分页,使用Redis缓存后再使用分页，需要先清除缓存
                page = (Page<AttractionAdminAccount>) attractionAdminAccountList;
                redisTemplate.opsForValue().set("attractionAdminAccount_commonQuery" + "::" + attractionAdminAccountQuery.toString(), JSON.toJSONString(attractionAdminAccountList));
                // 缓存PageInfo（分页信息）
                Map<String, Integer> pageInfo = new HashMap<>();
                pageInfo.put("pageNum", page.getPageNum());
                pageInfo.put("pages", page.getPages());
                pageInfo.put("total", (int) page.getTotal());
                redisTemplate.opsForValue().set("attractionAdminAccount_commonQuery_page_info" + "::" + attractionAdminAccountQuery.toString(), JSON.toJSONString(pageInfo));
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
            } else {
                attractionAdminAccountList = JSON.parseArray(listStr).toJavaList(AttractionAdminAccount.class);
                Map<String, Integer> map = JSON.parseObject((String) redisTemplate.opsForValue().get("attractionAdminAccount_commonQuery_page_info" + "::" + attractionAdminAccountQuery.toString())).toJavaObject(Map.class);
                page = new Page<AttractionAdminAccount>();
                for (AttractionAdminAccount attractionAdminAccount1 : attractionAdminAccountList) {
                    page.add(attractionAdminAccount1);
                }
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo((long) map.get("total"), map.get("pages"))));
            }

            List<AttractionAdminAccountDTO> attractionAdminAccountDTOS = BeanUtil.copyList(attractionAdminAccountList, AttractionAdminAccountDTO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    // 调用 BeanUtil.copyProperties
                    if (o == null || o1 == null) {
                        throw new BeanException("拷贝对象不可以为空", new Exception("copyList 拷贝回调错误"));
                    }

                    AttractionAdminAccount attractionAdminAccount1 = (AttractionAdminAccount) o;
                    AttractionAdminAccountDTO attractionAdminAccountDTO = (AttractionAdminAccountDTO) o1;
                    BeanUtil.copy(attractionAdminAccount1.getAttraction(), attractionAdminAccountDTO.getAttractionDTO(), SupplierDTO.class);
                    BeanUtil.copy(attractionAdminAccount1.getSupplier(), attractionAdminAccountDTO.getSupplierDTO(), SupplierDTO.class);
                }
            });

            // 将查询所得实体结果列表转换为DTO对象列表
            return attractionAdminAccountDTOS;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_ATTRACTION_ADMIN_ACCOUNT_DB_EXCEPTION_CODE, ex);
        }

    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public Boolean update(AttractionAdminAccountDTO attractionAdminAccountDTO) {
        if (attractionAdminAccountDTO == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("景点管理员修改：参数为空"));
        }
        try {
            AttractionAdminAccount attractionAdminAccount = BeanUtil.copy(attractionAdminAccountDTO, AttractionAdminAccount.class);
            Example example = new Example(AttractionAdminAccount.class);
            example.createCriteria().andEqualTo("id", attractionAdminAccount.getId())
                    .andEqualTo("version", attractionAdminAccount.getVersion());
            // 批量清除缓存
            long delete = batchDeleteCache("attractionAdminAccount_commonQuery");
            return attractionAdminAccountMapper.updateByExample(attractionAdminAccount, example) > 0;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_ATTRACTION_ADMIN_ACCOUNT_DB_EXCEPTION_CODE, ex);
        }
    }

    @Override
    public AttractionAdminAccountDTO queryByAccount(String account) {
        AttractionAdminAccount attractionAdminAccount = attractionAdminAccountMapper.queryByAccount(account);
        if (attractionAdminAccount == null) {
            return null;
        }
        return BeanUtil.copy(attractionAdminAccount, AttractionAdminAccountDTO.class);
    }

    @Override
    public Boolean batchRemoveAttractionAdminAccount(DeleteAttractionAdminAccountDTO[] deleteAttractionAdminAccountDTOS) {
        Assert.notEmpty(deleteAttractionAdminAccountDTOS, "批量删除参数不可以为空");
        for (DeleteAttractionAdminAccountDTO deleteAttractionAdminAccountDTO : deleteAttractionAdminAccountDTOS) {
            Example example = new Example(AttractionAdminAccount.class);
            example.createCriteria().andEqualTo("id", deleteAttractionAdminAccountDTO.getId()).andEqualTo("version", deleteAttractionAdminAccountDTO.getVersion());
            int result = attractionAdminAccountMapper.deleteByExample(example);
            Assert.isTrue(result > 0, String.format("删除的记录id %d 没有匹配到版本", deleteAttractionAdminAccountDTO.getId()));
        }
        // 批量清除缓存
        long delete = batchDeleteCache("attractionAdminAccount_commonQuery");
        return true;
    }

    @Override
    public Boolean batchUpdateAttractionAdminAccount(AttractionAdminAccountDTO[] attractionAdminAccountDTOS) {
        Assert.noNullElements(attractionAdminAccountDTOS, "更新数组不能为空");
        for (AttractionAdminAccountDTO attractionAdminAccountDTO : attractionAdminAccountDTOS) {
            AttractionAdminAccount attractionAdminAccount = BeanUtil.copy(attractionAdminAccountDTO, AttractionAdminAccount.class);
            Example example = new Example(attractionAdminAccount.getClass());
            example.createCriteria().andEqualTo("id", attractionAdminAccount.getId()).andEqualTo("version", attractionAdminAccount.getVersion());
            int result = attractionAdminAccountMapper.updateByExampleSelective(attractionAdminAccount, example);
            Assert.isTrue(result > 0, "没有找到相应id更新记录");
        }
        // 批量清除缓存
        long delete = batchDeleteCache("attractionAdminAccount_commonQuery");
        return true;
    }

    /**
     * 根据key前缀批量删除缓存（缓存数据多的情况下性能差）
     *
     * @param key
     * @return
     */
    private Long batchDeleteCache(String key) {
        Set<String> keys = redisTemplate.keys(key + "*");
        return redisTemplate.delete(keys);
    }
}

package com.sd365.digital.village.travel.platform.basedata.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.aop.MyPageInfo;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.MemberMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.Member;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.MemberDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.MemberQuery;
import com.sd365.digital.village.travel.platform.basedata.service.MemberService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;


@Service
public class MemberServiceImpl implements MemberService {
    @Resource
    private MemberMapper memberMapper;
    @Resource
    private IdGenerator idGenerator;

    /**
     * 用户默认头像地址
     **/
    public static final String DEFAULT_AVATAR_URL = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fww2.sinaimg.cn%2Fmw690%2F0071ogI5gy1gj7s0sfweoj30u00u0jt1.jpg&refer=http%3A%2F%2Fwww.sina.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1612317101&t=69362b2c487a4f775a233b9f2b4933c8";

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Boolean add(MemberDTO memberDTO) {
        Member member = BeanUtil.copy(memberDTO, Member.class);
        member.setId(idGenerator.snowflakeId());
        /*if (file == null) {
            member.setAvatarUrl(DEFAULT_AVATAR_URL);
        } else {
            List<String> urlList = pictureService.fileUpload(file);
            member.setAvatarUrl(urlList.get(0));
        }*/
        return memberMapper.insert(member) > 0;
    }

    @Override
    public Boolean delete(Long id, Long version) {
        if (id == null || version == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("会员删除：id或version为null"));
        }
        Example example = new Example(Member.class);
        example.createCriteria().andEqualTo("id", id)
                .andEqualTo("version", version);
        return memberMapper.deleteByExample(example) > 0;
    }

    @Override
    public List<MemberDTO> commonQuery(MemberQuery memberQuery) {
        // 对查询条件进行判空
        if (memberQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("会员查询：查询条件非法"));
        }

        try {
            // 将查询条件转换为实体
            Member member = new Member();
            member.setNickName(memberQuery.getNickName());
            member.setName(memberQuery.getName());
            member.setSex(memberQuery.getSex());
            member.setAccount(memberQuery.getAccount());
            member.setStatus(memberQuery.getStatus());
            member.setBankAccount(memberQuery.getBankAccount());
            List<Member> memberList = memberMapper.commonQuery(member);
            // 获取分页信息
            Page<?> page = (Page<?>) memberList;
            // 将分页信息保存到基础框架的基本应用上下文中
            BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));

            // 将查询所得实体结果列表转换为DTO对象列表
            return BeanUtil.copyList(memberList, MemberDTO.class);
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_MEMBER_DB_EXCEPTION_CODE, ex);
        }
    }

    @Override
    public MemberDTO queryByAccount(String acount) {
        if (acount == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("会员账号查询：查询条件非法"));
        }
        Member member = memberMapper.queryByAccount(acount);
        if (member == null) {
            return null;
        }
        return BeanUtil.copy(member, MemberDTO.class);
    }

    @Override
    public MemberDTO queryById(Long id) {
        if (id == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("会员id查询：查询条件非法"));
        }
        Member member = memberMapper.selectByPrimaryKey(id);
        if (member == null) {
            return null;
        }
        return BeanUtil.copy(member, MemberDTO.class);
    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public Boolean update(MemberDTO memberDTO) {
        if (memberDTO == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("会员信息修改：参数为空"));
        }
        try {
            Member member = BeanUtil.copy(memberDTO, Member.class);
            Example example = new Example(Member.class);
            example.createCriteria().andEqualTo("id", member.getId())
                    .andEqualTo("version", member.getVersion());
            return memberMapper.updateByExample(member, example) > 0;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_MEMBER_DB_EXCEPTION_CODE, ex);
        }
    }
}

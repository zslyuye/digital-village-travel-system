package com.sd365.digital.village.travel.platform.basedata.service.impl;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.aop.MyPageInfo;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.AttractionMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.Attraction;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionQuery;
import com.sd365.digital.village.travel.platform.basedata.service.AttractionService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Service
public class AttractionServiceImpl implements AttractionService {

    @Autowired
    private AttractionMapper attractionMapper;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Long add(AttractionDTO attractionDTO) {
        Attraction attraction = BeanUtil.copy(attractionDTO, Attraction.class);
        long id = idGenerator.snowflakeId();
        attraction.setId(id);
        if (attractionMapper.insert(attraction) > 0) {
            // 这里是为了清除缓存才设置的id
            attractionDTO.setId(id);
            // 批量清除缓存
            long delete = batchDeleteCache("attraction_commonQuery");
            //批量清除缓存
            long delete1 = batchDeleteCache("attraction_pcCommonQuery");
            return attraction.getId();
        } else {
            return (long) -1;
        }
    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public Boolean modify(AttractionDTO attractionDTO) {
        try {
            Attraction attraction = BeanUtil.copy(attractionDTO, Attraction.class);
            Example example = new Example(Attraction.class);
            example.createCriteria().andEqualTo("id", attractionDTO.getId())
                    .andEqualTo("version", attractionDTO.getVersion());
            // 批量清除
            long delete = batchDeleteCache("attraction_commonQuery");
            // 批量清除
            long delete1 = batchDeleteCache("attraction_pcCommonQuery");
            return attractionMapper.updateByExampleSelective(attraction, example) > 0;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE, ex);

        }
    }

    @Override
    public List<AttractionDTO> commonQuery(AttractionQuery attractionQuery) {
        if (attractionQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("commonQuery 方法错误"));
        }

        try {

            Attraction attraction = new Attraction();
            attraction.setName(attractionQuery.getName());
            attraction.setType(attractionQuery.getType());
            attraction.setAddress(attractionQuery.getAddress());
            attraction.setMark(attractionQuery.getMark());
            attraction.setLevel(attractionQuery.getLevel());
            attraction.setAttractionSupplierId(attractionQuery.getAttractionSupplierId());
            attraction.setReferencePrice(attractionQuery.getReferencePrice());
            // 从redis缓存取出页数据
            String listStr = (String) redisTemplate.opsForValue().get("attraction_commonQuery" + "::" + attractionQuery.toString());
            List<Attraction> attractionList = null;
            Page<Attraction> page = null;
            if (listStr == null) {
                attractionList = attractionMapper.commonQuery(attraction);

                // 分页,使用Redis缓存后再使用分页，需要先清除缓存
                page = (Page<Attraction>) attractionList;
                redisTemplate.opsForValue().set("attraction_commonQuery" + "::" + attractionQuery.toString(), JSON.toJSONString(attractionList));
                // 缓存PageInfo（分页信息）
                Map<String, Integer> pageInfo = new HashMap<>();
                pageInfo.put("pageNum", page.getPageNum());
                pageInfo.put("pages", page.getPages());
                pageInfo.put("total", (int) page.getTotal());
                redisTemplate.opsForValue().set("attraction_commonQuery_page_info" + "::" + attractionQuery.toString(), JSON.toJSONString(pageInfo));
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
            } else {
                attractionList = JSON.parseArray(listStr).toJavaList(Attraction.class);
                Map<String, Integer> map = JSON.parseObject((String) redisTemplate.opsForValue().get("attraction_commonQuery_page_info" + "::" + attractionQuery.toString())).toJavaObject(Map.class);
                page = new Page<Attraction>();
                for (Attraction attraction1 : attractionList) {
                    page.add(attraction1);
                }
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo((long) map.get("total"), map.get("pages"))));
            }

            //对象转化 po list to dto list
            List<AttractionDTO> attractionDTOS = BeanUtil.copyList(attractionList, AttractionDTO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    // 调用 BeanUtil.copyProperties
                    if (o == null || o1 == null) {
                        throw new BeanException("拷贝对象不可以为空", new Exception("copyList 拷贝回调错误"));
                    }

                    Attraction attractionSource = (Attraction) o;
                    AttractionDTO attractionDTO = (AttractionDTO) o1;
                    BeanUtil.copy(attractionSource.getSupplier(), attractionDTO.getSupplierDTO(), SupplierDTO.class);
                    attractionDTO.setPictureDTOS(BeanUtil.copyList(attractionSource.getPictures(), PictureDTO.class));
                }
            });

            return attractionDTOS;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_ATTRACTION_DB_EXCEPTION_CODE, ex);

        }
    }

    @Override
    public List<AttractionDTO> pcCommonQuery(AttractionQuery attractionQuery) {
        if (attractionQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("commonQuery 方法错误"));
        }

        try {

            Attraction attraction = new Attraction();
            attraction.setName(attractionQuery.getName());
            attraction.setType(attractionQuery.getType());
            attraction.setAddress(attractionQuery.getAddress());
            attraction.setMark(attractionQuery.getMark());
            attraction.setStatus(attractionQuery.getStatus());
            attraction.setLevel(attractionQuery.getLevel());
            attraction.setAttractionSupplierId(attractionQuery.getAttractionSupplierId());
            attraction.setReferencePrice(attractionQuery.getReferencePrice());
            // 从redis缓存取出页数据
            String listStr = (String) redisTemplate.opsForValue().get("attraction_pcCommonQuery" + "::" + attractionQuery.toString());
            List<Attraction> attractionList = null;
            Page<Attraction> page = null;
            if (listStr == null) {
                attractionList = attractionMapper.pcCommonQuery(attraction);
                // 分页,使用Redis缓存后再使用分页，需要先清除缓存
                page = (Page<Attraction>) attractionList;
                redisTemplate.opsForValue().set("attraction_pcCommonQuery" + "::" + attractionQuery.toString(), JSON.toJSONString(attractionList));
                // 缓存PageInfo（分页信息）
                Map<String, Integer> pageInfo = new HashMap<>();
                pageInfo.put("pageNum", page.getPageNum());
                pageInfo.put("pages", page.getPages());
                pageInfo.put("total", (int) page.getTotal());
                redisTemplate.opsForValue().set("attraction_pcCommonQuery_page_info" + "::" + attractionQuery.toString(), JSON.toJSONString(pageInfo));
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
            } else {
                attractionList = JSON.parseArray(listStr).toJavaList(Attraction.class);
                Map<String, Integer> map = JSON.parseObject((String) redisTemplate.opsForValue().get("attraction_pcCommonQuery_page_info" + "::" + attractionQuery.toString())).toJavaObject(Map.class);
                page = new Page<Attraction>();
                for (Attraction attraction1 : attractionList) {
                    page.add(attraction1);
                }
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo((long) map.get("total"), map.get("pages"))));
            }
            //对象转化 po list to dto list
            List<AttractionDTO> attractionDTOS = BeanUtil.copyList(attractionList, AttractionDTO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    // 调用 BeanUtil.copyProperties
                    if (o == null || o1 == null) {
                        throw new BeanException("拷贝对象不可以为空", new Exception("copyList 拷贝回调错误"));
                    }

                    Attraction attractionSource = (Attraction) o;
                    AttractionDTO attractionDTO = (AttractionDTO) o1;
                    BeanUtil.copy(attractionSource.getSupplier(), attractionDTO.getSupplierDTO(), SupplierDTO.class);
                    attractionDTO.setPictureDTOS(BeanUtil.copyList(attractionSource.getPictures(), PictureDTO.class));
                }
            });

            return attractionDTOS;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_ATTRACTION_DB_EXCEPTION_CODE, ex);

        }
    }

    @Override
    public AttractionDTO queryAttractionById(Long id) {
        try {
            AttractionDTO attractionDTO = null;
            Attraction attraction = attractionMapper.selectById(id);
            if (attraction != null) {
                attractionDTO = BeanUtil.copy(attraction, AttractionDTO.class);
                BeanUtil.copy(attraction.getSupplier(), SupplierDTO.class);
                attractionDTO.setPictureDTOS(BeanUtil.copyList(attraction.getPictures(), PictureDTO.class));
            }
            return attractionDTO;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_ATTRACTION_DB_EXCEPTION_CODE, ex);
        }
    }

    @Override
    public Boolean remove(Long id, Long version) {
        Example example = new Example(Attraction.class);
        example.createCriteria().andEqualTo("id", id).andEqualTo("version", version);
        // 批量清除缓存
        long delete = batchDeleteCache("attraction_commonQuery");
        // 批量清除缓存
        long delete1 = batchDeleteCache("attraction_pcCommonQuery");
        return attractionMapper.deleteByExample(example) > 0;
    }

    @Override
    public Boolean batchUpdateAttraction(AttractionDTO[] attractionDTOS) {
        Assert.noNullElements(attractionDTOS, "更新数组不能为空");
        for (AttractionDTO attractionDTO : attractionDTOS) {
            Attraction attraction = BeanUtil.copy(attractionDTO, Attraction.class);
            Example example = new Example(attraction.getClass());
            example.createCriteria().andEqualTo("id", attraction.getId()).andEqualTo("version", attraction.getVersion());
            int result = attractionMapper.updateByExampleSelective(attraction, example);
            Assert.isTrue(result > 0, "没有找到相应id更新记录");
        }
        // 批量清除缓存
        long delete = batchDeleteCache("attraction_commonQuery");
        // 批量清除缓存
        long delete1 = batchDeleteCache("attraction_pcCommonQuery");
        return true;
    }

    @Override
    public Boolean modifyAttraction(Long id, Integer unSolveOrder, Integer customerSum, Float profitSum) {
        try {
            Attraction attraction = new Attraction();
            attraction.setId(id);
            attraction.setUnsolveOrder(unSolveOrder);
            attraction.setCustomerSum(customerSum);
            attraction.setProfitSum(profitSum);
            attraction.setStatus(null);
            Example example = new Example(Attraction.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("id", id);
            // 批量清除缓存
            long delete = batchDeleteCache("attraction_commonQuery");
            // 批量清除缓存
            long delete1 = batchDeleteCache("attraction_pcCommonQuery");
            return attractionMapper.updateByExampleSelective(attraction, example) > 0;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        }
    }

    /**
     * 根据key前缀批量删除缓存（缓存数据多的情况下性能差）
     *
     * @param key
     * @return
     */
    private Long batchDeleteCache(String key) {
        Set<String> keys = redisTemplate.keys(key + "*");
        return redisTemplate.delete(keys);
    }

}

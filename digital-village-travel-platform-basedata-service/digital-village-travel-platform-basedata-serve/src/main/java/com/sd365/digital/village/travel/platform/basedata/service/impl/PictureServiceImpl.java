package com.sd365.digital.village.travel.platform.basedata.service.impl;

import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.PictureMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.Picture;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeletePictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.PictureQuery;
import com.sd365.digital.village.travel.platform.basedata.service.PictureService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import com.sd365.digital.village.travel.platform.basedata.utils.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;


@Service
public class PictureServiceImpl implements PictureService {

    @Autowired
    private OssUtil ossUtil;

    @Autowired
    private PictureMapper pictureMapper;

    @Autowired
    private IdGenerator idGenerator;

    @Override
    public String fileUpload(MultipartFile file) {
        String pictureUrl ="";
        if (!file.isEmpty()){
            pictureUrl= ossUtil.uploadFile(file);
        }
        return pictureUrl;
    }

    @Override
    public List<String> fileListUpload(List<MultipartFile> fileList) {
        List<String> pictureUrlList = new ArrayList<>();
        if (fileList.size()>0){
            pictureUrlList = ossUtil.uploadFile(fileList);
        }
        return pictureUrlList;
    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Boolean add(PictureDTO pictureDTO) {
        Picture picture= BeanUtil.copy(pictureDTO,Picture.class);
        picture.setId(idGenerator.snowflakeId());
        return pictureMapper.insert(picture)>0 ;
    }

    @Override
    public Boolean remove(Long id, Long version) {
        Example example=new Example(Picture.class);
        example.createCriteria().andEqualTo("id",id).andEqualTo("version",version);
        return pictureMapper.deleteByExample(example)>0;
    }

    @Override
    public Boolean batchRemove(DeletePictureDTO[] deletePictureDTOS) {
        Assert.notEmpty(deletePictureDTOS,"批量删除参数不可以为空");
        for(DeletePictureDTO deletePictureDTO : deletePictureDTOS){
            Example example=new Example(Picture.class);
            example.createCriteria().andEqualTo("id",deletePictureDTO.getId()).andEqualTo("version",deletePictureDTO.getVersion()) ;
            int result= pictureMapper.deleteByExample(example);
            Assert.isTrue(result>0,String.format("删除的记录id %d 没有匹配到版本",deletePictureDTO.getId()));
        }
        return true;
    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public Boolean modify(PictureDTO pictureDTO) {
        try {
            Picture picture = BeanUtil.copy(pictureDTO, Picture.class);
            Example example=new Example(Picture.class);
            example.createCriteria().andEqualTo("id",pictureDTO.getId())
                    .andEqualTo("version", pictureDTO.getVersion());
            return pictureMapper.updateByExample(picture,example)>0 ;

        }catch (BeanException ex){
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION,ex);
        } catch (Exception ex){
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE,ex);

        }
    }

    @Override
    public List<PictureDTO> commonQuery(PictureQuery pictureQuery) {
        if(pictureQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID,new Exception("commonQuery 方法错误"));
        }
        try {
            Picture picture = new Picture();
            picture.setFkId(pictureQuery.getFkId());
            List<Picture> pictures= pictureMapper.commonQuery(picture);
            List<PictureDTO> pictureDTOS =  BeanUtil.copyList(pictures, PictureDTO.class);
            return pictureDTOS;
        } catch (BeanException ex){
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION,ex);
        } catch (Exception ex){
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE,ex);
        }
    }

    @Override
    public PictureDTO queryById(Long id) {
        try{
            PictureDTO  pictureDTO= null;
            Picture picture = pictureMapper.selectById(id);
            if(picture != null) {
                pictureDTO = BeanUtil.copy(picture, PictureDTO.class);
            }
            return pictureDTO;
        }catch (BeanException ex){
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION,ex);
        } catch (Exception ex){
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE,ex);
        }
    }
}

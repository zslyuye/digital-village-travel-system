package com.sd365.digital.village.travel.platform.basedata.service.exception;

import com.sd365.common.core.common.exception.code.IErrorCode;


public enum BasedataExceptionCode implements IErrorCode {
    /**
     * 错误码（先写一个例子，具体错误写的时候更新完善）
     * 命名格式：BUSINESS_BASEDATA_错误描述（可由多个关键字组成）_EXCEPTION_CODE
     * code格式：100***（6位）
     * 例：
     * BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE(100000, "用户参数异常"),
     * BUSINESS_BASEDATA_USER_NOTFOUND_EXCEPTION_CODE(100001, "用户查询结果为空异常"),
     */
    BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE(100000, "用户参数异常"),
    BUSINESS_BASEDATA_ATTRACTION_ADMIN_ACCOUNT_DB_EXCEPTION_CODE(100001, "景点管理员账号数据库异常"),
    BUSINESS_BASEDATA_HOTEL_ADMIN_ACCOUNT_DB_EXCEPTION_CODE(100002, "酒店管理员账号数据库异常"),
    BUSINESS_BASEDATA_ATTRACTION_DB_EXCEPTION_CODE(100003, "景点数据库异常"),
    BUSINESS_BASEDATA_HOTEL_DB_EXCEPTION_CODE(100004, "景点数据库异常"),
    BUSINESS_BASEDATA_ATTRACTION_TICKET_FOR_PLAN_DB_EXCEPTION_CODE(100005, "计划景点门票数据库异常"),
    BUSINESS_BASEDATA_HOTEL_ROOM_FOR_PLAN_DB_EXCEPTION_CODE(100006, "计划酒店房间数据库异常"),
    BUSINESS_BASEDATA_MEMBER_DB_EXCEPTION_CODE(100000, "会员数据库操作异常");


    /**
     * 错误码
     */
    private int code;
    /**
     * 错误消息
     */
    private String message;

    BasedataExceptionCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return 0;
    }

    @Override
    public String getMessage() {
        return null;
    }
}

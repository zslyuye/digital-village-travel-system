package com.sd365.digital.village.travel.platform.basedata.service.impl;

import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.HotelRoomForPlanMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.HotelRoomForPlan;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomForPlanDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelRoomForPlanQuery;
import com.sd365.digital.village.travel.platform.basedata.service.HotelRoomForPlanService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
public class HotelRoomForPlanServiceImpl implements HotelRoomForPlanService {

    @Autowired
    private HotelRoomForPlanMapper hotelRoomForPlanMapper;

    @Autowired
    private IdGenerator idGenerator;


    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Boolean add(HotelRoomForPlanDTO hotelRoomForPlanDTO) {
        HotelRoomForPlan hotelRoomForPlan = BeanUtil.copy(hotelRoomForPlanDTO, HotelRoomForPlan.class);
        hotelRoomForPlan.setId(idGenerator.snowflakeId());
        return hotelRoomForPlanMapper.insert(hotelRoomForPlan) > 0;
    }

    @Override
    public List<HotelRoomForPlanDTO> commonQuery(HotelRoomForPlanQuery hotelRoomForPlanQuery) {
        if (hotelRoomForPlanQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("commonQuery 方法错误"));
        }

        try {
            HotelRoomForPlan hotelRoomForPlan = new HotelRoomForPlan();
            hotelRoomForPlan.setHotelId(hotelRoomForPlanQuery.getHotelId());
            hotelRoomForPlan.setPlanId(hotelRoomForPlanQuery.getPlanId());
            List<HotelRoomForPlan> hotelRoomForPlans = hotelRoomForPlanMapper.commonQuery(hotelRoomForPlan);
            List<HotelRoomForPlanDTO> hotelRoomForPlanDTOS = BeanUtil.copyList(hotelRoomForPlans, HotelRoomForPlanDTO.class);

            return hotelRoomForPlanDTOS;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_HOTEL_ROOM_FOR_PLAN_DB_EXCEPTION_CODE, ex);

        }
    }
}

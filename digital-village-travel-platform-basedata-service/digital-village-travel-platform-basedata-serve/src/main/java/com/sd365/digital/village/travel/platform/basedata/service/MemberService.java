package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.MemberDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.MemberQuery;

import java.util.List;


public interface MemberService {

    Boolean add(MemberDTO memberDTO);

    Boolean delete(Long id, Long version);

    List<MemberDTO> commonQuery(MemberQuery memberQuery);

    MemberDTO queryByAccount(String acount);

    MemberDTO queryById(Long id);

    Boolean update(MemberDTO memberDTO);

}

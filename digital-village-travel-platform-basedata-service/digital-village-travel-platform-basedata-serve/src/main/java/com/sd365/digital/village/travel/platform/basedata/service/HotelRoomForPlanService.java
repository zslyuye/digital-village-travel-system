package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomForPlanDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelRoomForPlanQuery;

import java.util.List;


public interface HotelRoomForPlanService {

    /**
     * 添加定制计划酒店房间价格
     *
     * @param hotelRoomForPlanDTO
     * @return
     */
    Boolean add(HotelRoomForPlanDTO hotelRoomForPlanDTO);

    /**
     * 根据条件查询定制计划酒店房间价格
     *
     * @param hotelRoomForPlanQuery
     * @return
     */
    List<HotelRoomForPlanDTO> commonQuery(HotelRoomForPlanQuery hotelRoomForPlanQuery);
}

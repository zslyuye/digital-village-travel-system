package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteAttractionAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionAdminAccountQuery;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface AttractionAdminAccountService {
    Boolean add(AttractionAdminAccountDTO attractionAdminAccountDTO);

    Boolean delete(Long id, Long version);

    List<AttractionAdminAccountDTO> commonQuery(AttractionAdminAccountQuery attractionAdminAccountQuery);

    Boolean update(AttractionAdminAccountDTO attractionAdminAccountDTO);

    AttractionAdminAccountDTO queryByAccount(String account);

    /**
     * 批量删除景点管理员账号
     * @param deleteAttractionAdminAccountDTOS
     * @return
     */
    @Transactional
    Boolean batchRemoveAttractionAdminAccount(DeleteAttractionAdminAccountDTO[] deleteAttractionAdminAccountDTOS);

    /**
     * 批量修改景点管理员账号
     * @param attractionAdminAccountDTOS
     * @return
     */
    @Transactional
    Boolean batchUpdateAttractionAdminAccount(AttractionAdminAccountDTO[] attractionAdminAccountDTOS);

}

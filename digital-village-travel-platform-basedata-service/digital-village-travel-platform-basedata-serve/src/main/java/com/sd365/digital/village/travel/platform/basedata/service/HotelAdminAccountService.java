package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteHotelAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelAdminAccountQuery;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface HotelAdminAccountService {
    Boolean add(HotelAdminAccountDTO hotelAdminAccountDTO);

    Boolean delete(Long id, Long version);

    List<HotelAdminAccountDTO> commonQuery(HotelAdminAccountQuery hotelAdminAccountQuery);

    Boolean update(HotelAdminAccountDTO hotelAdminAccountDTO);

    HotelAdminAccountDTO queryByAccount(String account);

    /**
     * 批量删除酒店管理员账号
     * @param deleteHotelAdminAccountDTOS
     * @return
     */
    @Transactional
    Boolean batchRemoveHotelAdminAccount(DeleteHotelAdminAccountDTO[] deleteHotelAdminAccountDTOS);

    /**
     * 批量修改酒店管理员账号
     * @param hotelAdminAccountDTOS
     * @return
     */
    @Transactional
    Boolean batchUpdateHotelAdminAccount(HotelAdminAccountDTO[] hotelAdminAccountDTOS);

}

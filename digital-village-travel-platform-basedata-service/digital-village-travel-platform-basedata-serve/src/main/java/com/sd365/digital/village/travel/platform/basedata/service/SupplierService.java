package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.CheckSupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.SupplierQuery;

import java.util.List;


public interface SupplierService {
    /**
     * 添加新供应商
     *
     * @param supplierDTO 新的供应商DTO对象
     * @return java.lang.Boolean 是否添加成功
     **/
    Long add(SupplierDTO supplierDTO);

    /**
     * 移除供应商
     *
     * @param id      要移除的供应商id
     * @param version api版本
     * @return java.lang.Boolean 是否移除成果
     **/
    Boolean remove(Long id, Long version);

    /**
     * 修改供应商信息
     *
     * @param supplierDTO 供应商DTO对象
     * @return java.lang.Boolean 是否修改成功
     **/
    Boolean modify(SupplierDTO supplierDTO);

    /**
     * 审核供应商
     * @param checkSupplierDTO
     * @return
     */
    Boolean check(CheckSupplierDTO checkSupplierDTO);

    /**
     * 通用查询供应商
     *
     * @param supplierQuery 查询条件
     * @return java.util.List<SupplierDTO> 查询结果集
     **/
    List<SupplierDTO> commonQuery(SupplierQuery supplierQuery);

    /**
     * 根据id查询供应商信息
     * @param id
     * @return SupplierDTO
     */
    SupplierDTO queryById(Long id);

    /**
     * 根据账号查询供应商信息，用于登录验证
     * @param account
     * @return
     */
    SupplierDTO queryByAccount(String account);
}

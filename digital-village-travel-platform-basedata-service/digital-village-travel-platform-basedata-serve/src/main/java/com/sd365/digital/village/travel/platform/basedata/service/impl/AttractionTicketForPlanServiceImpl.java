package com.sd365.digital.village.travel.platform.basedata.service.impl;

import cn.hutool.core.lang.Assert;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.AttractionTicketForPlanMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.AttractionTicketForPlan;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionTicketForPlanDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionTicketForPlanQuery;
import com.sd365.digital.village.travel.platform.basedata.service.AttractionTicketForPlanService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AttractionTicketForPlanServiceImpl implements AttractionTicketForPlanService {

    @Autowired
    private AttractionTicketForPlanMapper attractionTicketForPlanMapper;

    @Autowired
    private IdGenerator idGenerator;

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Boolean add(AttractionTicketForPlanDTO attractionTicketForPlanDTO) {
        AttractionTicketForPlan attractionTicketForPlan = BeanUtil.copy(attractionTicketForPlanDTO, AttractionTicketForPlan.class);
        attractionTicketForPlan.setId(idGenerator.snowflakeId());
        return attractionTicketForPlanMapper.insert(attractionTicketForPlan) > 0;
    }

    @Override
    public Boolean batchAdd(AttractionTicketForPlanDTO[] attractionTicketForPlanDTOS) {
        Assert.noNullElements(attractionTicketForPlanDTOS, "添加的数组不能为空");
        for (AttractionTicketForPlanDTO attractionTicketForPlanDTO : attractionTicketForPlanDTOS) {
            AttractionTicketForPlan attractionTicketForPlan = BeanUtil.copy(attractionTicketForPlanDTO, AttractionTicketForPlan.class);
            attractionTicketForPlan.setId(idGenerator.snowflakeId());
            int result = attractionTicketForPlanMapper.insert(attractionTicketForPlan);
            Assert.isTrue(result > 0, "添加数据失败！");
        }
        return true;
    }

    @Override
    public List<AttractionTicketForPlanDTO> commonQuery(AttractionTicketForPlanQuery attractionTicketForPlanQuery) {
        if (attractionTicketForPlanQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("commonQuery 方法错误"));
        }
        try {
            AttractionTicketForPlan attractionTicketForPlan = new AttractionTicketForPlan();
            attractionTicketForPlan.setAttractionId(attractionTicketForPlanQuery.getAttractionId());
            attractionTicketForPlan.setPlanId(attractionTicketForPlanQuery.getPlanId());
            attractionTicketForPlan.setType(attractionTicketForPlanQuery.getType());

            List<AttractionTicketForPlan> attractionTicketForPlans = attractionTicketForPlanMapper.commonQuery(attractionTicketForPlan);
            List<AttractionTicketForPlanDTO> attractionTicketForPlanDTOS = BeanUtil.copyList(attractionTicketForPlans, AttractionTicketForPlanDTO.class);
            return attractionTicketForPlanDTOS;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_ATTRACTION_TICKET_FOR_PLAN_DB_EXCEPTION_CODE, ex);

        }
    }
}

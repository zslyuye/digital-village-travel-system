package com.sd365.digital.village.travel.platform.basedata.service.impl;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.aop.MyPageInfo;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BusinessErrorCode;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.digital.village.travel.platform.basedata.dao.mapper.HotelMapper;
import com.sd365.digital.village.travel.platform.basedata.entity.Hotel;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelQuery;
import com.sd365.digital.village.travel.platform.basedata.service.HotelService;
import com.sd365.digital.village.travel.platform.basedata.service.exception.BasedataExceptionCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Service
public class HotelServiceImpl implements HotelService {

    @Autowired
    private HotelMapper hotelMapper;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Long add(HotelDTO hotelDTO) {
        Hotel hotel = BeanUtil.copy(hotelDTO, Hotel.class);
        long id = idGenerator.snowflakeId();
        if (hotelMapper.insert(hotel) > 0) {
            // 这里是为了清除缓存才设置的id
            hotel.setId(id);
            // 批量清除缓存
            long delete = batchDeleteCache("hotel_commonQuery");
            //批量清除缓存
            long delete1 = batchDeleteCache("hotel_pcCommonQuery");
            return hotel.getId();
        } else {
            return (long) -1;
        }
    }

    @Override
    public Boolean remove(Long id, Long version) {
        Example example = new Example(Hotel.class);
        example.createCriteria().andEqualTo("id", id).andEqualTo("version", version);
        // 批量清除缓存
        long delete = batchDeleteCache("hotel_commonQuery");
        //批量清除缓存
        long delete1 = batchDeleteCache("hotel_pcCommonQuery");
        return hotelMapper.deleteByExample(example) > 0;
    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public Boolean modify(HotelDTO hotelDTO) {
        try {
            Hotel hotel = BeanUtil.copy(hotelDTO, Hotel.class);
            Example example = new Example(Hotel.class);
            example.createCriteria().andEqualTo("id", hotelDTO.getId())
                    .andEqualTo("version", hotelDTO.getVersion());
            // 批量清除缓存
            long delete = batchDeleteCache("hotel_commonQuery");
            //批量清除缓存
            long delete1 = batchDeleteCache("hotel_pcCommonQuery");
            return hotelMapper.updateByExample(hotel, example) > 0;

        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_USER_PARAM_EXCEPTION_CODE, ex);

        }
    }

    @Override
    public List<HotelDTO> commonQuery(HotelQuery hotelQuery) {
        if (hotelQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("commonQuery 方法错误"));
        }

        try {

            Hotel hotel = new Hotel();
            hotel.setName(hotelQuery.getName());
            hotel.setType(hotelQuery.getType());
            hotel.setAddress(hotelQuery.getAddress());
            hotel.setScore(hotelQuery.getScore());
            hotel.setStar(hotelQuery.getStar());
            hotel.setReferencePrice(hotelQuery.getReferencePrice());
            // 从redis缓存取出页数据
            String listStr = (String) redisTemplate.opsForValue().get("hotel_commonQuery" + "::" + hotelQuery.toString());
            List<Hotel> hotels = null;
            Page<Hotel> page = null;
            if (listStr == null) {
                hotels = hotelMapper.commonQuery(hotel);
                // 分页,使用Redis缓存后再使用分页，需要先清除缓存
                page = (Page<Hotel>) hotels;
                redisTemplate.opsForValue().set("hotel_commonQuery" + "::" + hotelQuery.toString(), JSON.toJSONString(hotels));
                // 缓存PageInfo（分页信息）
                Map<String, Integer> pageInfo = new HashMap<>();
                pageInfo.put("pageNum", page.getPageNum());
                pageInfo.put("pages", page.getPages());
                pageInfo.put("total", (int) page.getTotal());
                redisTemplate.opsForValue().set("hotel_commonQuery_page_info" + "::" + hotelQuery.toString(), JSON.toJSONString(pageInfo));
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
            } else {
                hotels = JSON.parseArray(listStr).toJavaList(Hotel.class);
                Map<String, Integer> map = JSON.parseObject((String) redisTemplate.opsForValue().get("hotel_commonQuery_page_info" + "::" + hotelQuery.toString())).toJavaObject(Map.class);
                page = new Page<Hotel>();
                for (Hotel hotel1 : hotels) {
                    page.add(hotel1);
                }
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo((long) map.get("total"), map.get("pages"))));
            }
            //对象转化 po list to dto list
            List<HotelDTO> hotelDTOS = BeanUtil.copyList(hotels, HotelDTO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    // 调用 BeanUtil.copyProperties
                    if (o == null || o1 == null) {
                        throw new BeanException("拷贝对象不可以为空", new Exception("copyList 拷贝回调错误"));
                    }

                    Hotel hotelSource = (Hotel) o;
                    HotelDTO hotelDTO = (HotelDTO) o1;
                    BeanUtil.copy(hotelSource.getSupplier(), hotelDTO.getSupplierDTO(), SupplierDTO.class);
                    hotelDTO.setPictureDTOS(BeanUtil.copyList(hotelSource.getPictures(), PictureDTO.class));
                }
            });

            return hotelDTOS;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_HOTEL_DB_EXCEPTION_CODE, ex);

        }
    }

    @Override
    public List<HotelDTO> pcCommonQuery(HotelQuery hotelQuery) {
        if (hotelQuery == null) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID, new Exception("commonQuery 方法错误"));
        }

        try {

            Hotel hotel = new Hotel();
            hotel.setName(hotelQuery.getName());
            hotel.setType(hotelQuery.getType());
            hotel.setAddress(hotelQuery.getAddress());
            hotel.setScore(hotelQuery.getScore());
            hotel.setStar(hotelQuery.getStar());
            hotel.setStatus(hotelQuery.getStatus());
            hotel.setReferencePrice(hotelQuery.getReferencePrice());
            hotel.setHotelSupplierId(hotelQuery.getHotelSupplierId());
            // 从redis缓存取出页数据
            String listStr = (String) redisTemplate.opsForValue().get("hotel_pcCommonQuery" + "::" + hotelQuery.toString());
            List<Hotel> hotels = null;
            Page<Hotel> page = null;
            if (listStr == null) {
                hotels = hotelMapper.pcCommonQuery(hotel);
                // 分页,使用Redis缓存后再使用分页，需要先清除缓存
                page = (Page<Hotel>) hotels;
                redisTemplate.opsForValue().set("hotel_pcCommonQuery" + "::" + hotelQuery.toString(), JSON.toJSONString(hotels));
                // 缓存PageInfo（分页信息）
                Map<String, Integer> pageInfo = new HashMap<>();
                pageInfo.put("pageNum", page.getPageNum());
                pageInfo.put("pages", page.getPages());
                pageInfo.put("total", (int) page.getTotal());
                redisTemplate.opsForValue().set("hotel_pcCommonQuery_page_info" + "::" + hotelQuery.toString(), JSON.toJSONString(pageInfo));
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
            } else {
                hotels = JSON.parseArray(listStr).toJavaList(Hotel.class);
                Map<String, Integer> map = JSON.parseObject((String) redisTemplate.opsForValue().get("hotel_pcCommonQuery_page_info" + "::" + hotelQuery.toString())).toJavaObject(Map.class);
                page = new Page<Hotel>();
                for (Hotel hotel1 : hotels) {
                    page.add(hotel1);
                }
                BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo((long) map.get("total"), map.get("pages"))));
            }
            //对象转化 po list to dto list
            List<HotelDTO> hotelDTOS = BeanUtil.copyList(hotels, HotelDTO.class, new BeanUtil.CopyCallback() {
                @Override
                public void copy(Object o, Object o1) {
                    // 调用 BeanUtil.copyProperties
                    if (o == null || o1 == null) {
                        throw new BeanException("拷贝对象不可以为空", new Exception("copyList 拷贝回调错误"));
                    }

                    Hotel hotelSource = (Hotel) o;
                    HotelDTO hotelDTO = (HotelDTO) o1;
                    BeanUtil.copy(hotelSource.getSupplier(), hotelDTO.getSupplierDTO(), SupplierDTO.class);
                    hotelDTO.setPictureDTOS(BeanUtil.copyList(hotelSource.getPictures(), PictureDTO.class));
                }
            });

            return hotelDTOS;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_HOTEL_DB_EXCEPTION_CODE, ex);

        }
    }

    @Override
    public HotelDTO queryHotelById(Long id) {
        try {
            HotelDTO hotelDTO = null;
            Hotel hotel = hotelMapper.selectById(id);
            if (hotel != null) {
                hotelDTO = BeanUtil.copy(hotel, HotelDTO.class);
                BeanUtil.copy(hotel.getSupplier(), SupplierDTO.class);
                hotelDTO.setPictureDTOS(BeanUtil.copyList(hotel.getPictures(), PictureDTO.class));
            }
            return hotelDTO;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        } catch (Exception ex) {
            throw new BusinessException(BasedataExceptionCode.BUSINESS_BASEDATA_ATTRACTION_DB_EXCEPTION_CODE, ex);
        }
    }

    @Override
    public Boolean batchUpdateHotel(HotelDTO[] hotelDTOS) {
        Assert.noNullElements(hotelDTOS, "更新数组不能为空");
        for (HotelDTO hotelDTO : hotelDTOS) {
            Hotel hotel = BeanUtil.copy(hotelDTO, Hotel.class);
            Example example = new Example(hotel.getClass());
            example.createCriteria().andEqualTo("id", hotel.getId()).andEqualTo("version", hotel.getVersion());
            int result = hotelMapper.updateByExampleSelective(hotel, example);
            Assert.isTrue(result > 0, "没有找到相应id更新记录");
        }
        // 批量清除缓存
        long delete = batchDeleteCache("hotel_commonQuery");
        //批量清除缓存
        long delete1 = batchDeleteCache("hotel_pcCommonQuery");
        return true;
    }

    @Override
    public Boolean modifyHotel(Long id, Integer unSolveOrder, Integer customerSum, Float profitSum) {
        try {
            Hotel hotel = new Hotel();
            hotel.setId(id);
            hotel.setUnsolveOrder(unSolveOrder);
            hotel.setCustomerSum(customerSum);
            hotel.setProfitSum(profitSum);
            hotel.setStatus(null);
            Example example = new Example(Hotel.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("id", id);
            // 批量清除缓存
            long delete = batchDeleteCache("hotel_commonQuery");
            //批量清除缓存
            long delete1 = batchDeleteCache("hotel_pcCommonQuery");
            return hotelMapper.updateByExampleSelective(hotel, example) > 0;
        } catch (BeanException ex) {
            throw new BusinessException(BusinessErrorCode.SYSTEM_BEAN_COPY_EXCEPTION, ex);
        }
    }

    /**
     * 根据key前缀批量删除缓存（缓存数据多的情况下性能差）
     *
     * @param key
     * @return
     */
    private Long batchDeleteCache(String key) {
        Set<String> keys = redisTemplate.keys(key + "*");
        return redisTemplate.delete(keys);
    }
}

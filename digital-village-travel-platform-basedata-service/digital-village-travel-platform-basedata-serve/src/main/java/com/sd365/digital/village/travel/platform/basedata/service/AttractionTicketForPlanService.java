package com.sd365.digital.village.travel.platform.basedata.service;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionTicketForPlanDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionTicketForPlanQuery;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface AttractionTicketForPlanService {

    /**
     * 添加定制计划景点门票价格
     *
     * @param attractionTicketForPlanDTO
     * @return
     */
    Boolean add(AttractionTicketForPlanDTO attractionTicketForPlanDTO);

    /**
     * 批量添加定制计划景点门票价格
     *
     * @param attractionTicketForPlanDTOS
     * @return
     */
    @Transactional
    Boolean batchAdd(AttractionTicketForPlanDTO[] attractionTicketForPlanDTOS);

    /**
     * 根据条件查询定制计划景点门票价格
     *
     * @param attractionTicketForPlanQuery
     * @return
     */
    List<AttractionTicketForPlanDTO> commonQuery(AttractionTicketForPlanQuery attractionTicketForPlanQuery);
}

package com.sd365.digital.village.travel.platform.basedata.api;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AddTemporaryDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeletePictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.PictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.PictureQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.PictureVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin
@Api(tags = "图片上传管理 ",value ="/digital/village/travel/system/platform/basedata/v1/picture")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/picture")
public interface PictureApi {

    /**
     * 上传单个图片并返回url,由于基础框架无法返回string，所以改成返回list
     * @param file
     * @return
     */
    @ApiOperation(tags="上传单个图片",value="")
    @PostMapping(value="")
    @ResponseBody
    List<String> fileUpload(@RequestParam("file") MultipartFile file);


    /**
     * 上传多个图片并返回url
     * @param fileList
     * @return
     */
    @ApiOperation(tags="上传多个图片",value="")
    @PostMapping(value="/batch/upload")
    @ResponseBody
    List<String> fileListUpload(@RequestParam("files") List<MultipartFile> fileList);

    /**
     * @decription 根据参数查询图片
     * @return 查询成功则返回所有查询历史
     */
    @ApiOperation(tags = "根据参数查询图片",
            value = "/selectPicture")
    @GetMapping("/selectPicture")
    @ResponseBody
    List<PictureVO> selectPicture(
            PictureQuery pictureQuery);
    /**
     * @decription 根据参数更新图片信息
     * @param pictureDTO
     * @return
     */
    @ApiOperation(tags = "修改图片信息", value = "")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify( @Valid @RequestBody PictureDTO pictureDTO);

    /**
     * @param: 图片DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */

    @ApiOperation(tags="添加图片",value="/add")
    @PostMapping(value="/add")
    @ResponseBody
    Boolean add(@Valid @RequestBody PictureDTO pictureDTO);

    /**
     * 批量上传图片
     * @param: addTemporaryDTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    @ApiOperation(tags="批量添加图片",value="/batch/add")
    @PostMapping(value="/batch/add")
    @ResponseBody
    Boolean batchAdd(@Valid @RequestBody AddTemporaryDTO addTemporaryDTO);

    /**
     * 通过图片id 查询图片
     * @param id
     * @return
     */
    @ApiOperation(tags="查询图片通过ID",value="")
    @GetMapping(value = "{id}")
    @ResponseBody
    PictureVO queryPictureById(@PathVariable("id") Long id);


    /**
     * 通过图片id和版本号删除图片
     * @param id
     * @param version
     * @return
     */
    @ApiOperation(tags="删除图片",value="")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required=true) @RequestParam("id") Long id,
                   @ApiParam(value="当前行版本",required = true) @RequestParam("version") Long version);

    /**
     * 批量删除图片
     * @param deletePictureDTOS
     * @return
     */
    @ApiOperation(tags="批量删除图片",value="/batch")
    @DeleteMapping(value="/batch")
    @ResponseBody
    Boolean batchRemove(@Valid @RequestBody DeletePictureDTO[] deletePictureDTOS);
}

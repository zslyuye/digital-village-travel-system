package com.sd365.digital.village.travel.platform.basedata.api;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteAttractionAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionAdminAccountQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionAdminAccountVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@Api(tags = "景点管理员管理 ", value = "/digital/village/travel/system/platform/basedata/v1/attraction/admin/account")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/attraction/admin/account")
public interface AttractionAdminAccountApi {
    @ApiOperation(tags = "增加景点管理员", value = "")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@RequestBody @Valid AttractionAdminAccountDTO attractionAdminAccountDTO);

    @ApiOperation(tags = "删除景点管理员", value = "")
    @DeleteMapping(value = "")
    @ResponseBody
    Boolean delete(Long id, Long version);

    @ApiOperation(tags = "查询景点管理员", value = "")
    @GetMapping(value = "")
    @ResponseBody
    List<AttractionAdminAccountVO> commonQuery(AttractionAdminAccountQuery attractionAdminAccountQuery);

    @ApiOperation(tags = "修改景点管理员", value = "")
    @PutMapping(value = "")
    @ResponseBody
    Boolean update(@RequestBody @Valid AttractionAdminAccountDTO attractionAdminAccountDTO);

    @ApiOperation(tags = "按账号名查询景点管理员", value = "")
    @GetMapping(value = "{account}")
    @ResponseBody
    AttractionAdminAccountVO queryByAccount(@PathVariable("account") String account);

    /**
     * 批量删除景点管理员账号
     * @param deleteAttractionAdminAccountDTOS
     * @return
     */
    @ApiOperation(tags = "批量删除景点管理员账号", value = "/batch")
    @DeleteMapping(value = "/batch")
    @ResponseBody
    Boolean batchRemoveAttractionAdminAccount(@Valid @RequestBody DeleteAttractionAdminAccountDTO[] deleteAttractionAdminAccountDTOS);

    /**
     * 批量修改景点管理员账号
     * @param attractionAdminAccountDTOS
     * @return
     */
    @ApiOperation(tags="批量修改景点管理员账号",value="")
    @PutMapping(value = "/batch")
    @ResponseBody
    Boolean batchUpdateAttractionAdminAccount(@Valid @RequestBody AttractionAdminAccountDTO [] attractionAdminAccountDTOS);
}

package com.sd365.digital.village.travel.platform.basedata.api;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionTicketForPlanDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionTicketForPlanQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionTicketForPlanVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin
@Api(tags = "定制计划景点门票价格管理 ", value = "/digital/village/travel/system/platform/basedata/v1/attraction/ticket/for/plan")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/attraction/ticket/for/plan")
public interface AttractionTicketForPlanApi {

    /**
     * 添加定制计划景点门票价格
     *
     * @param attractionTicketForPlanDTO
     * @return
     */
    @ApiOperation(tags = "添加定制计划景点门票价格", value = "")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@RequestBody @Valid AttractionTicketForPlanDTO attractionTicketForPlanDTO);

    /**
     * 批量添加定制计划景点门票价格
     *
     * @param attractionTicketForPlanDTOS
     * @return
     */
    @ApiOperation(tags = "添加定制计划景点门票价格", value = "")
    @PostMapping(value = "/batch")
    @ResponseBody
    Boolean batchAdd(@RequestBody @Valid AttractionTicketForPlanDTO[] attractionTicketForPlanDTOS);


    /**
     * 根据条件查询定制计划景点门票价格
     *
     * @param attractionTicketForPlanQuery
     * @return
     */
    @ApiOperation(tags = "查询定制计划景点门票价格", value = "")
    @GetMapping(value = "")
    @ResponseBody
    List<AttractionTicketForPlanVO> commonQuery(AttractionTicketForPlanQuery attractionTicketForPlanQuery);

}

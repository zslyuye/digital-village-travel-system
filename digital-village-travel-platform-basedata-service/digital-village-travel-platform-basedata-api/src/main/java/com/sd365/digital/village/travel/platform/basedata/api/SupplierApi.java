package com.sd365.digital.village.travel.platform.basedata.api;


import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AddTemporaryDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.CheckSupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.SupplierDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.SupplierQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.SupplierVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin
@Api(tags = "供应商管理 ",value ="/digital/village/travel/system/platform/basedata/v1/supplier")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/supplier")
public interface SupplierApi {


    /**
     * 添加供应商
     * @param addTemporaryDTO
     * @return
     */
    @ApiOperation(tags="增加供应商",value="")
    @PostMapping(value="")
    @ResponseBody
    Boolean add(@RequestBody @Valid AddTemporaryDTO addTemporaryDTO);


    /**
     * 删除景点
     * @param id
     * @param version
     * @return
     */
    @ApiOperation(tags="删除供应商",value="")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required=true) @RequestParam("id") Long id,
                   @ApiParam(value="当前行版本",required = true) @RequestParam("version") Long version);

    /**
     * 修改供应商信息
     * @param supplierDTO
     * @return
     */

    @ApiOperation(tags="修改供应商信息",value="")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify( @Valid @RequestBody  SupplierDTO supplierDTO);

    /**
     * 审核供应商
     * @param checkSupplierDTO
     * @return
     */

    @ApiOperation(tags="审核供应商",value="")
    @PutMapping(value = "/check")
    @ResponseBody
    Boolean check( @Valid @RequestBody CheckSupplierDTO checkSupplierDTO);


    /**
     * 根据条件查询供应商信息
     * @param supplierQuery
     * @return
     */
    @ApiOperation(tags="查询供应商",value="")
    @GetMapping(value = "")
    @ResponseBody
    List<SupplierVO> commonQuery(SupplierQuery supplierQuery);


    /**
     * 查询供应商通过ID
     * @param id
     * @return
     */
    @ApiOperation(tags="查询景点通过ID",value="")
    @GetMapping(value = "{id}")
    @ResponseBody
    SupplierVO queryById(@PathVariable("id") Long id);

    /**
     * 查询景点通过account
     * @param account
     * @return
     */
    @ApiOperation(tags="查询景点通过account",value="")
    @GetMapping(value = "account/{account}")
    @ResponseBody
    SupplierVO queryByAccount(@PathVariable("account") String account);
}

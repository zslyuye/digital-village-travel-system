package com.sd365.digital.village.travel.platform.basedata.api;


import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionTicketDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteAttractionTicketDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionTicketQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionTicketVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@Api(tags = "景点门票管理 ",value ="/digital/village/travel/system/platform/basedata/v1/attractionTicket")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/attractionTicket")
public interface AttractionTicketApi {
    /**
     * @decription 根据参数查询景点门票
     * @return 查询成功则返回所有查询历史
     */
    @ApiOperation(tags = "根据参数查询景点门票",
            value = "/selectAttractionTicket")
    @GetMapping("/selectAttractionTicket")
    @ResponseBody
    List<AttractionTicketVO> selectAttractionTicket(
            AttractionTicketQuery attractionTicketQuery);
    /**
     * @decription 根据参数更新景点门票
     * @param attractionTicketDTO
     * @return
     */
    @ApiOperation(tags = "修改景点门票", value = "")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify( @Valid @RequestBody AttractionTicketDTO attractionTicketDTO);

    /**
     * @param: 景点门票DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */

    @ApiOperation(tags="添加景点门票",value="")
    @PostMapping(value="")
    @ResponseBody
    Boolean add(@Valid @RequestBody AttractionTicketDTO attractionTicketDTO);

    /**
     * 通过门票id查询门票
     * @param id
     * @return
     */

    @ApiOperation(tags="查询景点门票通过ID",value="")
    @GetMapping(value = "{id}")
    @ResponseBody
    AttractionTicketVO queryAttractionTicketById(@PathVariable("id") Long id);

    /**
     * 删除景点门票
     * @param id
     * @param version
     * @return
     */
    @ApiOperation(tags="删除景点门票",value="")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required=true) @RequestParam("id") Long id,
                   @ApiParam(value="当前行版本",required = true) @RequestParam("version") Long version);

    /**
     * 批量删除门票
     * @param deleteAttractionTicketDTOS
     * @return
     */
    @ApiOperation(tags="批量删除景点门票",value="/batch")
    @DeleteMapping(value="/batch")
    @ResponseBody
    Boolean batchRemove(@Valid @RequestBody DeleteAttractionTicketDTO[] deleteAttractionTicketDTOS);
}

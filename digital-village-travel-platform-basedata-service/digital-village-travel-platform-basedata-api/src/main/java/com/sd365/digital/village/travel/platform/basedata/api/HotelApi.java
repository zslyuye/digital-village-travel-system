package com.sd365.digital.village.travel.platform.basedata.api;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AddTemporaryDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin
@Api(tags = "酒店管理 ",value ="/digital/village/travel/system/platform/basedata/v1/hotel/info")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/hotel/info")
public interface HotelApi {

    /**
     * 添加酒店
     * @param addTemporaryDTO
     * @return
     */
    @ApiOperation(tags="增加酒店",value="")
    @PostMapping(value="")
    @ResponseBody
    Boolean add(@RequestBody @Valid AddTemporaryDTO addTemporaryDTO);


    /**
     * 删除酒店
     * @param id
     * @param version
     * @return
     */
    @ApiOperation(tags="删除酒店",value="")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required=true) @RequestParam("id") Long id,
                   @ApiParam(value="当前行版本",required = true) @RequestParam("version") Long version);

    /**
     * 修改酒店信息
     * @param hotelDTO
     * @return
     */

    @ApiOperation(tags="修改酒店",value="")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify( @Valid @RequestBody HotelDTO hotelDTO);


    /**
     * 根据条件查询酒店信息
     * @param hotelQuery
     * @return
     */
    @ApiOperation(tags="查询酒店",value="")
    @GetMapping(value = "")
    @ResponseBody
    List<HotelVO> commonQuery(HotelQuery hotelQuery);

    /**
     * pc根据条件查询酒店信息
     * @param hotelQuery
     * @return
     */
    @ApiOperation(tags="查询酒店",value="")
    @GetMapping(value = "/pc")
    @ResponseBody
    List<HotelVO> pcCommonQuery(HotelQuery hotelQuery);

    /**
     * 查询酒店通过ID
     * @param id
     * @return
     */
    @ApiOperation(tags="查询酒店通过ID",value="")
    @GetMapping(value = "{id}")
    @ResponseBody
    HotelVO queryHotelById(@PathVariable("id") Long id);

    /**
     * 批量修改酒店
     * @param hotelDTOS
     * @return
     */
    @ApiOperation(tags="批量修改酒店",value="")
    @PutMapping(value = "/batch")
    @ResponseBody
    Boolean batchUpdateHotel(@Valid @RequestBody HotelDTO[] hotelDTOS);


    /**
     * 修改酒店的营业信息
     * @param id  酒店id
     * @param unSolveOrder 酒店未处理订单
     * @param customerSum 酒店顾客数量
     * @param profitSum 酒店收益总额
     * @return
     */
    @ApiOperation(tags="修改酒店营业信息",value="")
    @PutMapping(value = "/update")
    @ResponseBody
    Boolean modifyHotel(@Valid @RequestParam("id") Long id,
                             @Valid @RequestParam("unSolveOrder") Integer unSolveOrder,
                             @Valid @RequestParam("customerSum") Integer customerSum,
                             @Valid @RequestParam("profitSum") Float profitSum);
}

package com.sd365.digital.village.travel.platform.basedata.api;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.MemberDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.MemberQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.MemberVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin
@Api(tags = "会员管理 ", value = "/digital/village/travel/system/member/v1/member")
@RequestMapping(value = "/digital/village/travel/system/member/v1/member")
public interface MemberApi {
    @ApiOperation(tags = "增加会员", value = "")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@Valid MemberDTO memberDTO);

    @ApiOperation(tags = "删除会员", value = "")
    @DeleteMapping(value = "")
    @ResponseBody
    Boolean delete(Long id, Long version);

    @ApiOperation(tags = "查询会员", value = "")
    @GetMapping(value = "")
    @ResponseBody
    List<MemberVO> commonQuery(MemberQuery memberQuery);

    @ApiOperation(tags = "按账号查询会员", value = "")
    @GetMapping(value = "account/{account}")
    @ResponseBody
    MemberVO queryByAccount(@PathVariable("account") String account);

    @ApiOperation(tags = "按id查询会员", value = "")
    @GetMapping(value = "{id}")
    @ResponseBody
    MemberVO queryById(@PathVariable("id") Long id);

    @ApiOperation(tags = "更新会员", value = "")
    @PutMapping(value = "")
    @ResponseBody
    Boolean update(@RequestBody @Valid MemberDTO memberDTO);
}

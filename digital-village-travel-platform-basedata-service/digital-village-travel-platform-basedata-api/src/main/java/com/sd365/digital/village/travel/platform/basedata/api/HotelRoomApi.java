package com.sd365.digital.village.travel.platform.basedata.api;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomAndPictureDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelRoomQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelRoomVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin
@Api(tags = "酒店房间管理 ",value ="/digital/village/travel/system/platform/basedata/v1/hotel")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/hotel")
public interface HotelRoomApi {

    /**
     * 增加酒店的房间信息，参数是所增加房间的相关信息
     * @param hotelRoomDTO
     * @return
     */
    @ApiOperation(tags="增加酒店房间",value="")
    @PostMapping(value="")
    @ResponseBody
    Boolean addRoom(@RequestBody @Valid HotelRoomAndPictureDTO hotelRoomDTO);

    /**
     * 修改酒店的某个房间信息
     * @param hotelRoomDTO
     * @return
     */
    @ApiOperation(tags="修改某个酒店房间",value="")
    @PutMapping(value="")
    @ResponseBody
    Boolean modifyRoom(@Valid @RequestBody HotelRoomDTO hotelRoomDTO);

    /**
     * 订房间后房间数量减少count
     * @param hotelId, id
     * @return
     */
    @ApiOperation(tags="订房间后房间数量减少count",value="/decrease")
    @PutMapping(value="/decrease")
    @ResponseBody
    Boolean decreaseRoom(@Valid @RequestParam("hotelId") Long hotelId, @Valid @RequestParam("roomId") Long id, @Valid @RequestParam("count") Integer count);

    /**
     * 删除酒店的某个房间信息
     * @param hotelRoomDTO
     * @return
     */
    @ApiOperation(tags="删除某个酒店房间信息",value="")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean deleteRoom(@RequestBody @Valid HotelRoomDTO hotelRoomDTO);

    /**
     * 根据条件(或者无条件，即查询全部)查询酒店的房间信息
     * @param hotelRoomQuery
     * @return
     */
    @ApiOperation(tags="根据日期查询某个酒店房间信息",value="")
    @GetMapping(value="")
    @ResponseBody
    List<HotelRoomVO> queryRoom(HotelRoomQuery hotelRoomQuery);
}

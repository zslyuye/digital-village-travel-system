package com.sd365.digital.village.travel.platform.basedata.api;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelRoomForPlanDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelRoomForPlanQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelRoomForPlanVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;



@CrossOrigin
@Api(tags = "定制计划酒店房间价格管理 ", value = "/digital/village/travel/system/platform/basedata/v1/hotel/room/for/plan")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/hotel/room/for/plan")
public interface HotelRoomForPlanApi {

    /**
     * 添加定制计划酒店房间价格
     *
     * @param hotelRoomForPlanDTO
     * @return
     */
    @ApiOperation(tags = "添加定制计划酒店房间价格", value = "")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@RequestBody @Valid HotelRoomForPlanDTO hotelRoomForPlanDTO);

    /**
     * 根据条件查询定制计划酒店房间价格
     *
     * @param hotelRoomForPlanQuery
     * @return
     */
    @ApiOperation(tags = "查询定制计划景点门票价格", value = "")
    @GetMapping(value = "")
    @ResponseBody
    List<HotelRoomForPlanVO> commonQuery(HotelRoomForPlanQuery hotelRoomForPlanQuery);
}

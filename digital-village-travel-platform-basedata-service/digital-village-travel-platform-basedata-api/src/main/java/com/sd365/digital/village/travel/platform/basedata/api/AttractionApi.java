package com.sd365.digital.village.travel.platform.basedata.api;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AddTemporaryDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.AttractionDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.AttractionQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.AttractionVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@Api(tags = "景点管理 ",value ="/digital/village/travel/system/platform/basedata/v1/attraction")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/attraction")
public interface AttractionApi {

    /**
     * 添加景点
     * @param addTemporaryDTO
     * @return
     */
    @ApiOperation(tags="增加景点",value="")
    @PostMapping(value="")
    @ResponseBody
    Boolean add(@RequestBody @Valid AddTemporaryDTO addTemporaryDTO);


    /**
     * 删除景点
     * @param id
     * @param version
     * @return
     */
    @ApiOperation(tags="删除景点",value="")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required=true) @RequestParam("id") Long id,
                   @ApiParam(value="当前行版本",required = true) @RequestParam("version") Long version);

    /**
     * 修改景点信息
     * @param attractionDTO
     * @return
     */

    @ApiOperation(tags="修改景点",value="")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify( @Valid @RequestBody  AttractionDTO attractionDTO);


    /**
     * 根据条件查询景点信息
     * @param attractionQuery
     * @return
     */
    @ApiOperation(tags="查询景点",value="")
    @GetMapping(value = "")
    @ResponseBody
    List<AttractionVO> commonQuery(AttractionQuery attractionQuery);

    /**
     * pc端根据条件查询景点信息
     * @param attractionQuery
     * @return
     */
    @ApiOperation(tags="查询景点",value="/pc")
    @GetMapping(value = "/pc")
    @ResponseBody
    List<AttractionVO> pcCommonQuery(AttractionQuery attractionQuery);

    /**
     * 查询景点通过ID
     * @param id
     * @return
     */
    @ApiOperation(tags="查询景点通过ID",value="")
    @GetMapping(value = "{id}")
    @ResponseBody
    AttractionVO queryAttractionById(@PathVariable("id") Long id);

    /**
     * 批量修改景点
     * @param attractionDTOS
     * @return
     */
    @ApiOperation(tags="批量修改景点",value="")
    @PutMapping(value = "/batch")
    @ResponseBody
    Boolean batchUpdateAttraction(@Valid @RequestBody AttractionDTO[] attractionDTOS);

    /**
     * 修改景点的营业信息
     * @param id  景点id
     * @param unSolveOrder 景点未处理订单
     * @param customerSum 景点顾客数量
     * @param profitSum 景点收益总额
     * @return
     */
    @ApiOperation(tags="修改景点营业信息",value="")
    @PutMapping(value = "/update")
    @ResponseBody
    Boolean modifyAttraction(@Valid @RequestParam("id") Long id,
                             @Valid @RequestParam("unSolveOrder") Integer unSolveOrder,
                             @Valid @RequestParam("customerSum") Integer customerSum,
                             @Valid @RequestParam("profitSum") Float profitSum);

}

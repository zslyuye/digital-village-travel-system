package com.sd365.digital.village.travel.platform.basedata.api;

import com.sd365.digital.village.travel.platform.basedata.pojo.dto.DeleteHotelAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.dto.HotelAdminAccountDTO;
import com.sd365.digital.village.travel.platform.basedata.pojo.query.HotelAdminAccountQuery;
import com.sd365.digital.village.travel.platform.basedata.pojo.vo.HotelAdminAccountVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin
@Api(tags = "酒店管理员管理 ", value = "/digital/village/travel/system/platform/basedata/v1/hotel/admin/account")
@RequestMapping(value = "/digital/village/travel/system/platform/basedata/v1/hotel/admin/account")
public interface HotelAdminAccountApi {
    @ApiOperation(tags = "酒店景点管理员", value = "")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@RequestBody @Valid HotelAdminAccountDTO hotelAdminAccountDTO);

    @ApiOperation(tags = "删除酒店管理员", value = "")
    @DeleteMapping(value = "")
    @ResponseBody
    Boolean delete(Long id, Long version);

    @ApiOperation(tags = "查询酒店管理员", value = "")
    @GetMapping(value = "")
    @ResponseBody
    List<HotelAdminAccountVO> commonQuery(HotelAdminAccountQuery hotelAdminAccountQuery);

    @ApiOperation(tags = "修改酒店管理员", value = "")
    @PutMapping(value = "")
    @ResponseBody
    Boolean update(@RequestBody @Valid HotelAdminAccountDTO hotelAdminAccountDTO);

    @ApiOperation(tags = "按账号名查询酒店管理员", value = "")
    @GetMapping(value = "{account}")
    @ResponseBody
    HotelAdminAccountVO queryByAccount(@PathVariable("account") String account);

    /**
     * 批量删除酒店管理员账号
     * @param deleteHotelAdminAccountDTOS
     * @return
     */
    @ApiOperation(tags = "批量删除酒店管理员账号", value = "/batch")
    @DeleteMapping(value = "/batch")
    @ResponseBody
    Boolean batchRemoveHotelAdminAccount(@Valid @RequestBody DeleteHotelAdminAccountDTO[] deleteHotelAdminAccountDTOS);

    /**
     * 批量修改酒店管理员账号
     * @param hotelAdminAccountDTOS
     * @return
     */
    @ApiOperation(tags="批量修改酒店管理员账号",value="")
    @PutMapping(value = "/batch")
    @ResponseBody
    Boolean batchUpdateHotelAdminAccount( @Valid @RequestBody HotelAdminAccountDTO [] hotelAdminAccountDTOS);
}
